<?php

use Illuminate\Support\Facades\Route;
//login page
Route::get('/', 'LoginController@index')->name('index')->middleware('checkAdminLogin');
Route::post('login-check', 'LoginController@loginCheck')->name('loginCheck')->middleware('checkAdminLogin');
Route::middleware(['checkAdmin'])->prefix('admin')->group(function () {
//login check for admin
Route::get('dashboard', 'SuperAdminController@superAdminDashboard')->name('superAdminDashboard');
Route::get('superAdminLogout', 'SuperAdminController@superAdminLogout')->name('superAdminLogout');
Route::get('driver/create', 'AdminDriverController@driverCreate')->name('driverCreate');
Route::get('driver', 'AdminDriverController@allDriver')->name('allDriver');
Route::post('driver/save-driver', 'AdminDriverController@saveDriver')->name('saveDriver');
Route::get('driver/{id}', 'AdminDriverController@viewDriver')->name('viewDriver');
Route::get('driver-edit/{id}', 'AdminDriverController@editDriver')->name('editDriver');
Route::post('driver/update-driver', 'AdminDriverController@updateDriver')->name('updateDriver');
Route::post('driver-inactive', 'AdminDriverController@driverInactive')->name('driverInactive');
Route::post('driver-active', 'AdminDriverController@driverActive')->name('driverActive');
Route::post('driver-delete', 'AdminDriverController@driverDelete')->name('driverDelete');

Route::get('user', 'AdminUserController@showUser')->name('showUser');
Route::get('user/create', 'AdminUserController@userCreate')->name('userCreate');
Route::get('user/view/{id}', 'AdminUserController@userView')->name('userView');
Route::get('user/login-as/{id}', 'AdminUserController@userLoginAs')->name('userLoginAs');
Route::get('user/edit/{id}', 'AdminUserController@userEdit')->name('userEdit');
Route::post('user/update', 'AdminUserController@userUpdate')->name('userUpdate');
Route::post('user-inactive', 'AdminUserController@userInactive')->name('userInactive');
Route::post('user-active', 'AdminUserController@userActive')->name('userActive');
Route::post('user-delete', 'AdminUserController@userDelete')->name('userDelete');
Route::post('user/save', 'AdminUserController@userSave')->name('userSave');
Route::get('user/role', 'AdminUserController@showUserRole')->name('showUserRole');
Route::get('user/role-create', 'AdminUserController@userRoleCreate')->name('userRoleCreate');
Route::post('user/role-save', 'AdminUserController@userRoleSave')->name('userRoleSave');
Route::get('user/permission', 'AdminUserController@showUserPermission')->name('showUserPermission');
Route::get('user/permission/create', 'AdminUserController@createUserPermission')->name('createUserPermission');
Route::post('user-permission-inactive', 'AdminUserController@userPermissionInactive')->name('userPermissionInactive');
Route::post('user-permission-active', 'AdminUserController@userPermissionActive')->name('userPermissionActive');
Route::post('user/permission/save', 'AdminUserController@saveUserPermission')->name('saveUserPermission');
Route::get('user/permission/edit/{id}', 'AdminUserController@editUserPermission')->name('editUserPermission');
Route::post('user/permission/update', 'AdminUserController@updateUserPermission')->name('updateUserPermission');

Route::post('user-role-inactive', 'AdminUserController@userRoleInactive')->name('userRoleInactive');
Route::post('user-role-active', 'AdminUserController@userRoleActive')->name('userRoleActive');
Route::get('user-role-edit/{id}', 'AdminUserController@userRoleEdit')->name('userRoleEdit');
Route::post('user-role/update', 'AdminUserController@userRoleUpdate')->name('userRoleUpdate');

Route::get('log-viewer', 'AdminSystemLogController@showLogDashboard')->name('showLogDashboard');
Route::get('log-viewer/logs', 'AdminSystemLogController@showAllLog')->name('showAllLog');
Route::get('company/create', 'AdminCompanyController@createCompany')->name('createCompany');
Route::get('company', 'AdminCompanyController@allCompany')->name('allCompany');
Route::post('company/save-company', 'AdminCompanyController@saveCompany')->name('saveCompany');
Route::get('company/{id}', 'AdminCompanyController@viewCompany')->name('viewCompany');
Route::get('company-edit/{id}', 'AdminCompanyController@editCompany')->name('editCompany');
Route::post('company/update-company', 'AdminCompanyController@updateCompany')->name('updateCompany');
Route::post('company-inactive', 'AdminCompanyController@companyInactive')->name('companyInactive');
Route::post('company-active', 'AdminCompanyController@companyActive')->name('companyActive');

Route::get('branch/create', 'AdminBranchController@createBranch')->name('createBranch');
Route::get('branch', 'AdminBranchController@allBranch')->name('allBranch');
Route::get('assign/branch', 'AdminBranchController@assignBranch')->name('assignBranch');
Route::get('assign/branch/create', 'AdminBranchController@assignBranchCreate')->name('assignBranchCreate');
Route::post('assign/branch/save', 'AdminBranchController@assignBranchSave')->name('assignBranchSave');
Route::post('assign/branch/update', 'AdminBranchController@assignBranchUpdate')->name('assignBranchUpdate');
Route::post('assign/branch/delete', 'AdminBranchController@assignBranchDelete')->name('assignBranchDelete');
Route::get('assign/branch/edit/{id}', 'AdminBranchController@assignBranchEdit')->name('assignBranchEdit');
Route::post('branch/save-branch', 'AdminBranchController@saveBranch')->name('saveBranch');
Route::get('branch/{id}', 'AdminBranchController@viewBranch')->name('viewBranch');
Route::get('branch-edit/{id}', 'AdminBranchController@editBranch')->name('editBranch');
Route::post('branch/update-branch', 'AdminBranchController@updateBranch')->name('updateBranch');
Route::post('branch-inactive', 'AdminBranchController@branchInactive')->name('branchInactive');
Route::post('branch-active', 'AdminBranchController@branchActive')->name('branchActive');
Route::post('branch-delete', 'AdminBranchController@branchDelete')->name('branchDelete');

Route::get('employee/create', 'AdminEmployeeController@createEmployee')->name('createEmployee');
Route::get('employee', 'AdminEmployeeController@allEmployee')->name('allEmployee');
Route::post('employee/save-employee', 'AdminEmployeeController@saveEmployee')->name('saveEmployee');
Route::get('employee/{id}', 'AdminEmployeeController@viewEmployee')->name('viewEmployee');
Route::get('employee-edit/{id}', 'AdminEmployeeController@editEmployee')->name('editEmployee');
Route::post('employee/update-employee', 'AdminEmployeeController@updateEmployee')->name('updateEmployee');
Route::post('employee-inactive', 'AdminEmployeeController@employeeInactive')->name('employeeInactive');
Route::post('employee-active', 'AdminEmployeeController@employeeActive')->name('employeeActive');
Route::post('employee-delete', 'AdminEmployeeController@employeeDelete')->name('employeeDelete');
Route::get('fuel-rate/create', 'AdminFuelController@createFuelRate')->name('createFuelRate');
Route::get('fuel-rate', 'AdminFuelController@allFuelRate')->name('allFuelRate');
Route::post('fuel-rate/save-fuel-rate', 'AdminFuelController@saveFuelRate')->name('saveFuelRate');
Route::get('fuel-rate/{id}', 'AdminFuelController@viewFuelRate')->name('viewFuelRate');
Route::get('fuel-rate-edit/{id}', 'AdminFuelController@editFuelRate')->name('editFuelRate');
Route::post('fuel-rate/update-fuel-rate', 'AdminFuelController@updateFuelRate')->name('updateFuelRate');
Route::get('vehicle/assign', 'AdminVehicleController@assignVehicle')->name('assignVehicle');
Route::get('vehicle/assign/create', 'AdminVehicleController@assignVehicleCreate')->name('assignVehicleCreate');
Route::post('vehicle/assign/save', 'AdminVehicleController@assignVehicleSave')->name('assignVehicleSave');
Route::post('vehicle/assign/delete', 'AdminVehicleController@assignVehicleDelete')->name('assignVehicleDelete');
Route::post('vehicle/assign/update', 'AdminVehicleController@assignVehicleUpdate')->name('assignVehicleUpdate');
Route::get('vehicle/assign/edit/{id}', 'AdminVehicleController@assignVehicleEdit')->name('assignVehicleEdit');
Route::get('vehicle/assign/view/{id}', 'AdminVehicleController@assignVehicleView')->name('assignVehicleView');

Route::get('vehicle/create', 'AdminVehicleController@createVehicle')->name('createVehicle');
Route::get('vehicle', 'AdminVehicleController@allVehicle')->name('allVehicle');
Route::post('vehicle/save-vehicle', 'AdminVehicleController@saveVehicle')->name('saveVehicle');
Route::get('vehicle/{id}', 'AdminVehicleController@viewVehicle')->name('viewVehicle');
Route::get('vehicle-edit/{id}', 'AdminVehicleController@editVehicle')->name('editVehicle');
Route::post('vehicle/update-vehicle', 'AdminVehicleController@updateVehicle')->name('updateVehicle');
Route::post('vehicle-inactive', 'AdminVehicleController@vehicleInactive')->name('vehicleInactive');
Route::post('vehicle-active', 'AdminVehicleController@vehicleActive')->name('vehicleActive');
Route::post('vehicle-delete', 'AdminVehicleController@vehicleDelete')->name('vehicleDelete');
Route::get('vehicle_driver/create', 'AdminVehicleController@vehicleDriverCreate')->name('vehicleDriverCreate');
});
