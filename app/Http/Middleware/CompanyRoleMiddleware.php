<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use DB;
if(!isset($_SESSION)){ 
    session_start(); 
}
class CompanyRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $adminId=Session::get('adminId');
        $roleInfo=DB::table('role')
                        ->where('adminId',$adminId)
                        ->first();
        if ($roleInfo->companyRoleId=='1') {
           return redirect()->route('createCompany'); 
        }
        return $next($request);
    }
}
