<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminFuelController extends Controller
{

    public function createFuelRate(){
    	return view("admin.fuel.createFuel");
    }
    public function allFuelRate(){
    	$fuelRateInfo=DB::table('fuel_rates')
    						->get();
    	return view("admin.fuel.allFuel",compact("fuelRateInfo"));
    }
    public function saveFuelRate(Request $request){
    	$request->validate([
		    'branchName' => 'required',
		    'entryDate' => 'required',
		    'deiselRate' => 'required',
		    'petrolRate' => 'required',
		    'octaneRate' => 'required',
		    'cngRate' => 'required',
		]);

		$data=array();
		$data['branchName']=$request->branchName;
		$data['entryDate']=$request->entryDate;
		$data['deiselRate']=$request->deiselRate;
		$data['petrolRate']=$request->petrolRate;
		$data['octaneRate']=$request->octaneRate;
		$data['cngRate']=$request->cngRate;
		$orderNumber=$request->orderNumber;
		if ($orderNumber) {
			$data['orderNumber']=$request->orderNumber;
		}else{
			$data['orderNumber']=rand();
		}
		$data['status']=$request->status;
		$insertFuelRate=DB::table('fuel_rates')
        					->insert($data);
        if ($insertFuelRate) {
        	Session::put('message','Fuel rate create successfully done !!');
            return redirect()->back();
        }else{
        	Session::put('messageWarning','Fuel rate create failed !!');
            return redirect()->back();
        }
    }

    public function editFuelRate($id){
		$singleFuelRateInfo=DB::table('fuel_rates')
								->where('id',$id)
								->first();
		return view("admin.fuel.editFuel",compact("singleFuelRateInfo","id"));
	}

	public function viewFuelRate($id){
		$singleFuelRateInfo=DB::table('fuel_rates')
								->where('id',$id)
								->first();
		return view("admin.fuel.viewFuel",compact("singleFuelRateInfo","id"));
	}
	public function updateFuelRate(Request $request){
		$id=$request->id;
    	$request->validate([
		    'branchName' => 'required',
		    'entryDate' => 'required',
		    'deiselRate' => 'required',
		    'petrolRate' => 'required',
		    'octaneRate' => 'required',
		    'cngRate' => 'required',
		]);

		$data=array();
		$data['branchName']=$request->branchName;
		$data['entryDate']=$request->entryDate;
		$data['deiselRate']=$request->deiselRate;
		$data['petrolRate']=$request->petrolRate;
		$data['octaneRate']=$request->octaneRate;
		$data['cngRate']=$request->cngRate;
		$orderNumber=$request->orderNumber;
		if ($orderNumber) {
			$data['orderNumber']=$request->orderNumber;
		}else{
			$data['orderNumber']=rand();
		}
		$data['status']=$request->status;
		$updateFuelRate=DB::table('fuel_rates')
							->where('id',$id)
        					->update($data);
    	Session::put('message','Fuel rate update successfully done !!');
        return redirect()->back();
        
    }

}
