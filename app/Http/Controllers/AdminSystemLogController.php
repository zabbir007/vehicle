<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminSystemLogController extends Controller
{
    // public function __construct(){
    //     $this->middleware('checkAdmin');
    // }

    public function showLogDashboard(){
    	return view("admin.systemLog.showLogDashboard");
    }

    public function showAllLog(){
    	return view("admin.systemLog.showAllLog");
    }
}
