<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminEmployeeController extends Controller
{

    public function createEmployee(){
    	return view("admin.employee.createEmployee");
    }
    public function allEmployee(){
    	$employeeInfo=DB::table('employees')
    						->get();
    	return view("admin.employee.allEmployee",compact("employeeInfo"));
    }
    public function saveEmployee(Request $request){
    	$request->validate([
		    'companyId' => 'required',
		    'firstName' => 'required',
		    'designation' => 'required',
		    'email' => 'required',
		    'lastName' => 'required',
		    'department' => 'required',
		    'mobile' => 'required',
		]);

		$data=array();
		$data['companyId']=$request->companyId;
		$data['firstName']=$request->firstName;
		$data['designation']=$request->designation;
		$data['email']=$request->email;
		$data['employeeId']=$request->employeeId;
		$data['lastName']=$request->lastName;
		$data['department']=$request->department;
		$data['mobile']=$request->mobile;
		$order=$request->order;
		if ($order) {
			$data['order']=$order;
		}else{
			$data['order']=rand();
		}
		$data['status']=$request->status;
		$data['updateDate']=date('Y-m-d');
		$loginStatus=$request->loginStatus;
		if ($loginStatus) {
			$data['loginStatus']=$request->loginStatus;
			$data['userName']=$request->userName;
			$data['password']= md5($request->password);
		}

		$insertEmployee=DB::table('employees')
	        					->insert($data);
        if ($insertEmployee) {
        	Session::put('message','Employee create successfully done !!');
            return redirect()->back();
        }else{
        	Session::put('messageWarning','Employee create failed !!');
            return redirect()->back();
        }
    }

    public function viewEmployee($id){
		$singleEmployeeInfo=DB::table('employees')
								->where('id',$id)
								->first();
		return view("admin.employee.viewEmployee",compact("singleEmployeeInfo","id"));
	}
	public function editEmployee($id){
		$singleEmployeeInfo=DB::table('employees')
								->where('id',$id)
								->first();
		return view("admin.employee.editEmployee",compact("singleEmployeeInfo","id"));
	}

    public function employeeInactive(Request $request){
		$id=$request->id;
		$data=array();
		$data['status']=0;
		$inactive=DB::table('employees')
						->where('id',$id)
						->update($data);
	}

	public function employeeActive(Request $request){
		$id=$request->id;
		$data=array();
		$data['status']=1;
		$inactive=DB::table('employees')
						->where('id',$id)
						->update($data);
	}

	public function employeeDelete(Request $request){
		$id=$request->id;
		$delete=DB::table('employees')
					->where('id',$id)
					->delete();
	}

	public function updateEmployee(Request $request){
		$id=$request->id;
    	$request->validate([
		    'companyId' => 'required',
		    'firstName' => 'required',
		    'designation' => 'required',
		    'email' => 'required',
		    'lastName' => 'required',
		    'department' => 'required',
		    'mobile' => 'required',
		]);
		$data=array();
		$data['companyId']=$request->companyId;
		$data['firstName']=$request->firstName;
		$data['designation']=$request->designation;
		$data['email']=$request->email;
		$data['employeeId']=$request->employeeId;
		$data['lastName']=$request->lastName;
		$data['department']=$request->department;
		$data['mobile']=$request->mobile;
		$order=$request->order;
		if ($order) {
			$data['order']=$order;
		}else{
			$data['order']=rand();
		}
		$data['status']=$request->status;
		$data['updateDate']=date('Y-m-d');
		// echo "<pre/>";
		// print_r($id);
		// exit();
		$updateEmployee=DB::table('employees')
								->where('id',$id)
	        					->update($data);
        
    	Session::put('message','Employee update successfully done !!');
        return redirect()->back();
        
    }


}
