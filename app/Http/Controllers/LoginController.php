<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class LoginController extends Controller
{
	
    public function index(){
    	return view("login.loginPage");
    }

    public function loginCheck(Request $request){
    	$request->validate([
		    'userName' => 'required|max:30',
		    'password' => 'required',
		]);
		$userName=$request->userName;
		$password=md5($request->password);
		$loginCheck=DB::table("admin")
						->where("userName",$userName)
						->where("password",$password)
						->first();
		if ($loginCheck) {
			$roleFind=DB::table('role_admin as rd')
							->join('roles as r','rd.roleId','=','r.id')
							->where('rd.userId',$loginCheck->id)
							->select('r.*')
							->first();
		    $rolePermissionInfo=  explode(',',$roleFind->rolePermission);
            foreach($rolePermissionInfo as $rolePermission){
                Session::put('accessValue'.$rolePermission,$rolePermission);
            }
			Session::put('all',$roleFind->all);
			Session::put('adminId',$loginCheck->id);
			Session::put('adminUserName',$loginCheck->userName);
			return redirect()->route('superAdminDashboard');
		}else{
			Session::put('message','Username or Password Invalid');
			return redirect()->back();
		}
    }
}
