<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminUserController extends Controller
{
    

    public function showUser(){
        $UserInfo=DB::table('admin as ad')
                        ->join('role_admin as rd','ad.id','=','rd.userId')
                        ->join('roles as r','rd.roleId','=','r.id')
                        ->select('rd.userId','rd.roleId','r.roleName','ad.*')
                        ->get();
    	return view("admin.user.showUser",compact("UserInfo"));
    }

    public function userEdit($id){
        $userInfo=DB::table('admin as ad')
                        ->join('role_admin as rd','ad.id','=','rd.userId')
                        ->join('roles as r','rd.roleId','=','r.id')
                        ->select('rd.userId','rd.roleId','r.roleName','ad.*')
                        ->where('ad.id',$id)
                        ->first();
        $roleInfo=DB::table('roles')
                        ->get();
        return view("admin.user.editUser",compact("userInfo","roleInfo"));
    }

    public function userView($id){
        $userInfo=DB::table('admin as ad')
                        ->join('role_admin as rd','ad.id','=','rd.userId')
                        ->join('roles as r','rd.roleId','=','r.id')
                        ->select('rd.userId','rd.roleId','r.roleName','ad.*')
                        ->where('ad.id',$id)
                        ->first();
        $roleInfo=DB::table('roles')
                        ->get();
        return view("admin.user.viewUser",compact("userInfo","roleInfo"));
    }

    public function userInactive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=0;
        $inactive=DB::table('admin')
                        ->where('id',$id)
                        ->update($data);
    }

    public function userActive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=1;
        $inactive=DB::table('admin')
                        ->where('id',$id)
                        ->update($data);
    }

    public function userDelete(Request $request){
        $id=$request->id;
        $delete=DB::table('admin')
                    ->where('id',$id)
                    ->delete();
    }
    public function userUpdate(Request $request){
        $id=$request->id;
        $request->validate([
            'userName' => 'required',
            'email' => 'required',
            'roleId' => 'required',
        ]);
        $data=array();
        $data['userName']=$request->userName;
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['status']=$request->status;
        $data['lastUpdate']=date('Y-m-d');
        
        $updateAdmin=DB::table('admin')
                        ->where('id',$id)
                        ->update($data);
        $dataRole=array();
        $dataRole['userId']=$id;
        $dataRole['roleId']=$request->roleId;
        $updateRole=DB::table('role_admin')
                    ->where('userId',$id)
                    ->update($dataRole);
        Session::put('message','User update successfully done !!');
        return redirect()->back();
       

    }
    public function showUserRole(){
        $userRoleInfo=DB::table('roles')
                        ->get();
    	return view("admin.user.showUserRole",compact("userRoleInfo"));
    }

    public function showUserPermission(){
        $permissionInfo=DB::table('user_permission')
                                ->get();
    	return view("admin.user.showUserPermission",compact('permissionInfo'));
    }
    public function editUserPermission($id){
        $permissionInfo=DB::table('user_permission')
                        ->where('id',$id)
                        ->first();
        return view("admin.user.editUserPermission",compact("permissionInfo"));
    }
    public function userRoleCreate(){
        $permissionInfo=DB::table('user_permission')
                            ->get();
        return view("admin.user.userRoleCreate",compact("permissionInfo"));
    }

    public function userRoleInactive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=0;
        $inactive=DB::table('roles')
                        ->where('id',$id)
                        ->update($data);
    }

    public function userPermissionInactive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=0;
        $inactive=DB::table('user_permission')
                        ->where('id',$id)
                        ->update($data);
    }
    public function userRoleEdit($id){
        $singleUserRoleInfo=DB::table('roles')
                                ->where('id',$id)
                                ->first();
        $permissionInfo=DB::table('user_permission')
                            ->get();
        return view("admin.user.userRoleEdit",compact("singleUserRoleInfo","id","permissionInfo"));
    }
    public function userRoleActive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=1;
        $inactive=DB::table('roles')
                        ->where('id',$id)
                        ->update($data);
    }

    public function userPermissionActive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=1;
        $inactive=DB::table('user_permission')
                        ->where('id',$id)
                        ->update($data);
    }

    public function createUserPermission(){
        return view("admin.user.createUserPermission");
    }

    public function saveUserPermission(Request $request){
        $request->validate([
            'permission' => 'required',
            'identifier' => 'required',
        ]);
        $data=array();
        $data['permission']=$request->permission;
        $data['identifire']=$request->identifier;
        $data['lastUpdate']=date('Y-m-d');
        $data['status']=$request->status;
        $insertPermission=DB::table('user_permission')
                                ->insert($data);
        if ($insertPermission) {
            Session::put('message','Permission create successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Permission create failed !!');
            return redirect()->back();
        }
    }

    public function updateUserPermission(Request $request){
        $id=$request->id;
        $request->validate([
            'permission' => 'required',
            'identifier' => 'required',
        ]);
        $data=array();
        $data['permission']=$request->permission;
        $data['identifire']=$request->identifier;
        $data['lastUpdate']=date('Y-m-d');
        $data['status']=$request->status;
        $updatePermission=DB::table('user_permission')
                                ->where('id',$id)
                                ->update($data);
        
            Session::put('message','Permission update successfully done !!');
            return redirect()->back();
        
    } 
    public function userLoginAs($id){
        Session::flush();
        $loginCheck=DB::table("admin")
                        ->where('id',$id)
                        ->first();
        if ($loginCheck) {
            $roleFind=DB::table('role_admin as rd')
                            ->join('roles as r','rd.roleId','=','r.id')
                            ->where('rd.userId',$loginCheck->id)
                            ->select('r.*')
                            ->first();
            $rolePermissionInfo=  explode(',',$roleFind->rolePermission);
            foreach($rolePermissionInfo as $rolePermission){
                Session::put('accessValue'.$rolePermission,$rolePermission);
            }
            Session::put('all',$roleFind->all);
            Session::put('adminId',$loginCheck->id);
            Session::put('adminUserName',$loginCheck->userName);
            return redirect()->route('superAdminDashboard');
        }
        
    }

    public function userRoleSave(Request $request){
        $request->validate([
            'roleName' => 'required',
        ]);
        $data=array();
        $rolePermissionCheck=$request->rolePermission;
        if ($rolePermissionCheck) {
            $data['rolePermission']=implode(',',$request->rolePermission);
        }
        $data['all']=$request->all;
        $data['roleName']=$request->roleName;
        $data['status']=$request->status;
        $data['updateDate']=date('Y-m-d');
        $insertRole=DB::table('roles')
                        ->insert($data);
        if ($insertRole) {
            Session::put('message','Role create successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Role create failed !!');
            return redirect()->back();
        }
    }

    public function userRoleUpdate(Request $request){
        $id=$request->id;
        $request->validate([
            'roleName' => 'required',
        ]);
        $data=array();
        $rolePermissionCheck=$request->rolePermission;
        if ($rolePermissionCheck) {
            $data['rolePermission']=implode(',',$request->rolePermission);
        }else{
            $data['rolePermission']='';
        }
        $data['all']=$request->all;
        $data['roleName']=$request->roleName;
        $data['status']=$request->status;
        $data['updateDate']=date('Y-m-d');
        $data['status']=$request->status;
        $data['updateDate']=date('Y-m-d');
        $updateRole=DB::table('roles')
                        ->where('id',$id)
                        ->update($data);
        Session::put('message','Role update successfully done !!');
        return redirect()->back();
        
    }

    public function userCreate(){
        $roleInfo=DB::table('roles')
                        ->get();
        return view("admin.user.userCreate",compact("roleInfo"));
    }

    public function userSave(Request $request){
        $request->validate([
            'userName' => 'required',
            'email' => 'required',
            'password' => 'required',
            'roleId' => 'required',
        ]);
        $data=array();
        $data['userName']=$request->userName;
        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['status']=$request->status;
        $data['password']=md5($request->password);
        $data['lastUpdate']=date('Y-m-d');
        
        $insertAdmin=DB::table('admin')
                        ->insertGetId($data);
        if ($insertAdmin) {
            $dataRole=array();
            $dataRole['userId']=$insertAdmin;
            $dataRole['roleId']=$request->roleId;
            $insertRole=DB::table('role_admin')
                        ->insert($dataRole);
            Session::put('message','User create successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','User create failed !!');
            return redirect()->back();
        }

    }
}
