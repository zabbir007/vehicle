<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class SuperAdminController extends Controller
{
    // public function __construct(){
         
    //     $this->middleware('checkAdmin');
    // }

    public function superAdminDashboard(){
    	return view("admin.dashboard");
    }
    public function superAdminLogout(){
    	Session::put('adminId','');
        return redirect()->route('index');
    }
}
