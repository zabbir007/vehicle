<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminDriverController extends Controller
{

    public function driverCreate(){
    	return view("admin.driver.create");
    }
    public function allDriver(){
    	$driverInfo=DB::table('drivers')
    						->get();
    	return view("admin.driver.allDriver",compact("driverInfo"));
    }
    public function saveDriver(Request $request){
    	$request->validate([
		    'name' => 'required',
		    'birth' => 'required',
		    'presentAddress' => 'required',
		    'driverId' => 'required',
		    'mobile' => 'required',
		    'joining' => 'required',
		    'permanentAddress' => 'required',
		]);

		$data=array();
		$data['name']=$request->name;
		$data['birth']=$request->birth;
		$data['blood']=$request->blood;
		$data['presentAddress']=$request->presentAddress;
		$data['driverId']=$request->driverId;
		$data['mobile']=$request->mobile;
		$data['joining']=$request->joining;
		$data['emMobile']=$request->emMobile;
		$data['permanentAddress']=$request->permanentAddress;
		$data['nidDocument']=$request->nidDocument;
		$data['nidIssuing']=$request->nidIssuing;
		$data['nidNumber']=$request->nidNumber;
		$data['nidExpire']=$request->nidExpire;
		$data['nidIssue']=$request->nidIssue;
		$data['drivingDocument']=$request->drivingDocument;
		$data['drivingIssuing']=$request->drivingIssuing;
		$data['drivingNumber']=$request->drivingNumber;
		$data['drivingExpire']=$request->drivingExpire;
		$data['drivingIssue']=$request->drivingIssue;
		$order=$request->orderNumber;
		if ($order) {
			$data['orderNumber']=$order;
		}else{
			$data['orderNumber']=rand();
		}
		$data['status']=$request->status;
		$data['updateDate']=date('Y-m-d');
		$image=$request->file('photo');
	        if ($image) {
	            $image_name=rand();
	            $ext=strtolower($image->getClientOriginalExtension());
	            $permited  = array('jpg', 'jpeg', 'png', 'pdf');
	            if (in_array($ext, $permited) === false){
	                Session::put('messageWarning','You can only upload '.implode(', ', $permited));
	                return redirect()->back();
	            }
	            $image_full_name=$image_name.'.'.$ext;
	            $upload_path='driver/img/';
	            $image_url=$upload_path.$image_full_name;
	            $success=$image->move($upload_path,$image_full_name);
	        if ($success) {
	            $data['photo']=$image_url;
	        }
	        
	        
	    }
		$insertDriver=DB::table('drivers')
    					->insert($data);
        if ($insertDriver) {
        	Session::put('message','Driver create successfully done !!');
            return redirect()->back();
        }else{
        	Session::put('messageWarning','Driver create failed !!');
            return redirect()->back();
        }
    }

    public function viewDriver($id){
		$singleDriverInfo=DB::table('drivers')
								->where('id',$id)
								->first();
		return view("admin.driver.viewDriver",compact("singleDriverInfo","id"));
	}
	public function editDriver($id){
		$singleDriverInfo=DB::table('drivers')
								->where('id',$id)
								->first();
		return view("admin.driver.editDriver",compact("singleDriverInfo","id"));
	}

    public function driverInactive(Request $request){
		$id=$request->id;
		$data=array();
		$data['status']=0;
		$inactive=DB::table('drivers')
						->where('id',$id)
						->update($data);
	}

	public function driverActive(Request $request){
		$id=$request->id;
		$data=array();
		$data['status']=1;
		$inactive=DB::table('drivers')
						->where('id',$id)
						->update($data);
	}

	public function driverDelete(Request $request){
		$id=$request->id;
		$delete=DB::table('drivers')
					->where('id',$id)
					->delete();
	}

	public function updateDriver(Request $request){
		$id=$request->id;
    	$request->validate([
		    'name' => 'required',
		    'birth' => 'required',
		    'presentAddress' => 'required',
		    'driverId' => 'required',
		    'mobile' => 'required',
		    'joining' => 'required',
		    'permanentAddress' => 'required',
		]);

		$data=array();
		$data['name']=$request->name;
		$data['birth']=$request->birth;
		$data['blood']=$request->blood;
		$data['presentAddress']=$request->presentAddress;
		$data['driverId']=$request->driverId;
		$data['mobile']=$request->mobile;
		$data['joining']=$request->joining;
		$data['emMobile']=$request->emMobile;
		$data['permanentAddress']=$request->permanentAddress;
		$data['nidDocument']=$request->nidDocument;
		$data['nidIssuing']=$request->nidIssuing;
		$data['nidNumber']=$request->nidNumber;
		$data['nidExpire']=$request->nidExpire;
		$data['nidIssue']=$request->nidIssue;
		$data['drivingDocument']=$request->drivingDocument;
		$data['drivingIssuing']=$request->drivingIssuing;
		$data['drivingNumber']=$request->drivingNumber;
		$data['drivingExpire']=$request->drivingExpire;
		$data['drivingIssue']=$request->drivingIssue;
		$order=$request->orderNumber;
		if ($order) {
			$data['orderNumber']=$order;
		}else{
			$data['orderNumber']=rand();
		}
		$data['status']=$request->status;
		$data['updateDate']=date('Y-m-d');
		$image=$request->file('photo');
	        if ($image) {
	            $image_name=rand();
	            $ext=strtolower($image->getClientOriginalExtension());
	            $permited  = array('jpg', 'jpeg', 'png', 'pdf');
	            if (in_array($ext, $permited) === false){
	                Session::put('messageWarning','You can only upload '.implode(', ', $permited));
	                return redirect()->back();
	            }
	            $image_full_name=$image_name.'.'.$ext;
	            $upload_path='driver/img/';
	            $image_url=$upload_path.$image_full_name;
	            $success=$image->move($upload_path,$image_full_name);
	        if ($success) {
	            $data['photo']=$image_url;
	        }
	        
	        
	    }
		$updateDriver=DB::table('drivers')
						->where('id',$id)
    					->update($data);
        
    	Session::put('message','Driver update successfully done !!');
        return redirect()->back();
        
    }
}
