<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminVehicleController extends Controller
{
    
    public function createVehicle(){
    	return view("admin.vehicle.createVehicle");
    }
    public function allVehicle(){
        $vehicleInfo=DB::table('vehicles')
                            ->get();
    	return view("admin.vehicle.allVehicle",compact("vehicleInfo"));
    }

    public function assignVehicleCreate(){
        $branchInfo=DB::table('branches')
                            ->get();
        $vehicleInfo=DB::table('vehicles')
                            ->get();
        $driverInfo=DB::table('drivers')
                            ->get();
        return view("admin.vehicle.assignVehicleCreate",compact("branchInfo","vehicleInfo","driverInfo"));
    }

    public function assignVehicleSave(Request $request){
        $request->validate([
            'branchId' => 'required',
            'vehicleId' => 'required',
            'driverId' => 'required',
            'takeDate' => 'required',
        ]);

        $data=array();
        $data['branchId']=$request->branchId;
        $data['vehicleId']=$request->vehicleId;
        $data['driverId']=$request->driverId;
        $data['takeDate']=$request->takeDate;
        $data['overDate']=$request->overDate;
        $data['status']=$request->status;
        $insertAssign=DB::table('assign_driver')
                                ->insert($data);
        if ($insertAssign) {
            Session::put('message','Driver assign successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Driver assign failed !!');
            return redirect()->back();
        }
    }

    public function assignVehicleUpdate(Request $request){
        $id=$request->id;
        $request->validate([
            'branchId' => 'required',
            'vehicleId' => 'required',
            'driverId' => 'required',
            'takeDate' => 'required',
        ]);

        $data=array();
        $data['branchId']=$request->branchId;
        $data['vehicleId']=$request->vehicleId;
        $data['driverId']=$request->driverId;
        $data['takeDate']=$request->takeDate;
        $data['overDate']=$request->overDate;
        $data['status']=$request->status;
        $updateAssign=DB::table('assign_driver')
                                ->where('id',$id)
                                ->update($data);
       
        Session::put('message','Driver assign update successfully done !!');
        return redirect()->back();
        
    }
    public function assignVehicle(){
        $assignInfo=DB::table('assign_driver as ad')
                        ->join('branches as b','b.id','ad.branchId')
                        ->join('vehicles as v','v.id','ad.vehicleId')
                        ->join('drivers as d','d.id','ad.driverId')
                        ->select('b.name as bName','v.name as vName','d.name as dName','ad.*')
                        ->get();
        // echo "<pre>";
        // print_r($assignInfo);
        // exit();
    	return view("admin.vehicle.assignVehicle",compact("assignInfo"));
    }
    public function vehicleDriverCreate(){
        return view("admin.vehicle.vehicleDriverCreate");
    }
    public function assignVehicleView($id){
        $branchInfo=DB::table('branches')
                            ->get();
        $vehicleInfo=DB::table('vehicles')
                            ->get();
        $driverInfo=DB::table('drivers')
                            ->get();
        $singleAssignInfo=DB::table('assign_driver as ad')
                            ->join('branches as b','b.id','ad.branchId')
                            ->join('vehicles as v','v.id','ad.vehicleId')
                            ->join('drivers as d','d.id','ad.driverId')
                            ->select('b.id as bId','v.id as vId','d.id as dId','ad.*')
                            ->where('ad.id',$id)
                            ->first();
        return view("admin.vehicle.vehicleDriverView",compact("singleAssignInfo","id","branchInfo","vehicleInfo","driverInfo"));
    }
    public function assignVehicleEdit($id){
        $branchInfo=DB::table('branches')
                            ->get();
        $vehicleInfo=DB::table('vehicles')
                            ->get();
        $driverInfo=DB::table('drivers')
                            ->get();
        $singleAssignInfo=DB::table('assign_driver as ad')
                            ->join('branches as b','b.id','ad.branchId')
                            ->join('vehicles as v','v.id','ad.vehicleId')
                            ->join('drivers as d','d.id','ad.driverId')
                            ->select('b.id as bId','v.id as vId','d.id as dId','ad.*')
                            ->where('ad.id',$id)
                            ->first();
        return view("admin.vehicle.vehicleDriverEdit",compact("singleAssignInfo","id","branchInfo","vehicleInfo","driverInfo"));
    }
    public function viewVehicle($id){
        $singleVehicleInfo=DB::table('vehicles')
                                ->where('id',$id)
                                ->first();
        return view("admin.vehicle.viewVehicle",compact("singleVehicleInfo","id"));
    }
    public function editVehicle($id){
        $singleVehicleInfo=DB::table('vehicles')
                                ->where('id',$id)
                                ->first();
        return view("admin.vehicle.editVehicle",compact("singleVehicleInfo","id"));
    }

    public function vehicleInactive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=0;
        $inactive=DB::table('vehicles')
                        ->where('id',$id)
                        ->update($data);
    }

    public function vehicleActive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=1;
        $inactive=DB::table('vehicles')
                        ->where('id',$id)
                        ->update($data);
    }

    public function vehicleDelete(Request $request){
        $id=$request->id;
        $delete=DB::table('vehicles')
                    ->where('id',$id)
                    ->delete();
    }

    public function saveVehicle(Request $request){
        $request->validate([
            'companyId' => 'required',
            'name' => 'required',
            'manfYear' => 'required',
            'licenseNo' => 'required',
            'chassisNo' => 'required',
            'vinNo' => 'required',
            'opeMill' => 'required',
            'vehicleType' => 'required',
            'model' => 'required',
            'engineNo' => 'required',
        ]);

        $data=array();
        $data['companyId']=$request->companyId;
        $data['name']=$request->name;
        $data['manfYear']=$request->manfYear;
        $data['weight']=$request->weight;
        $data['licenseNo']=$request->licenseNo;
        $data['chassisNo']=$request->chassisNo;
        $data['vinNo']=$request->vinNo;
        $data['opeMill']=$request->opeMill;
        $data['vehicleType']=$request->vehicleType;
        $data['manufacture']=$request->manufacture;
        $data['model']=$request->model;
        $data['lifeTime']=$request->lifeTime;
        $data['licenseYear']=$request->licenseYear;
        $data['engineNo']=$request->engineNo;
        $data['purDate']=$request->purDate;
        $data['fitDocumentType']=$request->fitDocumentType;
        $data['fitDocumentNum']=$request->fitDocumentNum;
        $data['fitIssue']=$request->fitIssue;
        $data['fitIssuing']=$request->fitIssuing;
        $data['fitExpire']=$request->fitExpire;

        $data['insDocumentType']=$request->insDocumentType;
        $data['insDocumentNum']=$request->insDocumentNum;
        $data['insIssue']=$request->insIssue;
        $data['insIssuing']=$request->insIssuing;
        $data['insExpire']=$request->insExpire;

        $data['taxDocumentType']=$request->taxDocumentType;
        $data['taxDocumentNum']=$request->taxDocumentNum;
        $data['taxIssue']=$request->taxIssue;
        $data['taxIssuing']=$request->taxIssuing;
        $data['taxExpire']=$request->taxExpire;

        $data['fuelEntryDate']=$request->fuelEntryDate;
        $data['dualTank']=$request->dualTank;
        $data['firstFuel']=$request->firstFuel;
        $data['firstCapacity']=$request->firstCapacity;
        $data['firstStandard']=$request->firstStandard;

        $data['secondFuel']=$request->secondFuel;
        $data['secondCapacity']=$request->secondCapacity;
        $data['secondStandard']=$request->secondStandard;
        $order=$request->order;
        if ($order) {
            $data['order']=$order;
        }else{
            $data['order']=rand();
        }
        $data['status']=$request->status;
        $insertVehicle=DB::table('vehicles')
                                ->insert($data);
        if ($insertVehicle) {
            Session::put('message','Vehicle create successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Vehicle create failed !!');
            return redirect()->back();
        }
    }

    public function updateVehicle(Request $request){
        $id=$request->id;
        $request->validate([
            'companyId' => 'required',
            'name' => 'required',
            'manfYear' => 'required',
            'licenseNo' => 'required',
            'chassisNo' => 'required',
            'vinNo' => 'required',
            'opeMill' => 'required',
            'vehicleType' => 'required',
            'model' => 'required',
            'engineNo' => 'required',
        ]);

        $data=array();
        $data['companyId']=$request->companyId;
        $data['name']=$request->name;
        $data['manfYear']=$request->manfYear;
        $data['weight']=$request->weight;
        $data['licenseNo']=$request->licenseNo;
        $data['chassisNo']=$request->chassisNo;
        $data['vinNo']=$request->vinNo;
        $data['opeMill']=$request->opeMill;
        $data['vehicleType']=$request->vehicleType;
        $data['manufacture']=$request->manufacture;
        $data['model']=$request->model;
        $data['lifeTime']=$request->lifeTime;
        $data['licenseYear']=$request->licenseYear;
        $data['engineNo']=$request->engineNo;
        $data['purDate']=$request->purDate;
        $data['fitDocumentType']=$request->fitDocumentType;
        $data['fitDocumentNum']=$request->fitDocumentNum;
        $data['fitIssue']=$request->fitIssue;
        $data['fitIssuing']=$request->fitIssuing;
        $data['fitExpire']=$request->fitExpire;

        $data['insDocumentType']=$request->insDocumentType;
        $data['insDocumentNum']=$request->insDocumentNum;
        $data['insIssue']=$request->insIssue;
        $data['insIssuing']=$request->insIssuing;
        $data['insExpire']=$request->insExpire;

        $data['taxDocumentType']=$request->taxDocumentType;
        $data['taxDocumentNum']=$request->taxDocumentNum;
        $data['taxIssue']=$request->taxIssue;
        $data['taxIssuing']=$request->taxIssuing;
        $data['taxExpire']=$request->taxExpire;

        $data['fuelEntryDate']=$request->fuelEntryDate;
        $data['dualTank']=$request->dualTank;
        $data['firstFuel']=$request->firstFuel;
        $data['firstCapacity']=$request->firstCapacity;
        $data['firstStandard']=$request->firstStandard;

        $data['secondFuel']=$request->secondFuel;
        $data['secondCapacity']=$request->secondCapacity;
        $data['secondStandard']=$request->secondStandard;
        $order=$request->order;
        if ($order) {
            $data['order']=$order;
        }else{
            $data['order']=rand();
        }
        $data['status']=$request->status;
        $updateVehicle=DB::table('vehicles')
                            ->where('id',$id)
                            ->update($data);
        
        Session::put('message','Vehicle update successfully done !!');
        return redirect()->back();
        
    }

}
