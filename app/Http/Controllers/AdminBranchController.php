<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class AdminBranchController extends Controller
{
    
    public function createBranch(){
    	return view("admin.branch.createBranch");
    }
    public function allBranch(){
        $branchInfo=DB::table('branches')
                        ->get();
    	return view("admin.branch.allBranch",compact("branchInfo"));
    }
    public function assignBranch(){
        $assignInfo=DB::table('assign_branch as ab')
                        ->join('employees as e','e.id','ab.employeeId')
                        ->join('branches as b','b.id','ab.branchId')
                        ->select('e.firstName','b.name','ab.*')
                        ->get();
    	return view("admin.branch.assignBranch",compact("assignInfo"));
    }

    public function assignBranchDelete(Request $request){
        $id=$request->id;
        $delete=DB::table('assign_branch')
                    ->where('id',$id)
                    ->delete();
    }

    public function viewBranch($id){
        $singleBranchInfo=DB::table('branches')
                                ->where('id',$id)
                                ->first();
        return view("admin.branch.viewBranch",compact("singleBranchInfo","id"));
    }

    public function assignBranchCreate(){
        $branchInfo=DB::table('branches')
                            ->get();
        $employeeInfo=DB::table('employees')
                            ->get();
        return view("admin.branch.assignBranchCreate",compact("branchInfo","employeeInfo"));
    }

    public function assignBranchSave(Request $request){
        $request->validate([
            'employeeId' => 'required',
            'branchId' => 'required',
        ]);

        $data=array();
        $data['employeeId']=$request->employeeId;
        $data['branchId']=$request->branchId;
        $data['status']=$request->status;
        $insert=DB::table('assign_branch')
                    ->insert($data);
        if ($insert) {
            Session::put('message','Branch assign successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Branch assign failed !!');
            return redirect()->back();
        }
    }

    public function assignBranchUpdate(Request $request){
        $id=$request->id;
        $request->validate([
            'employeeId' => 'required',
            'branchId' => 'required',
        ]);

        $data=array();
        $data['employeeId']=$request->employeeId;
        $data['branchId']=$request->branchId;
        $data['status']=$request->status;
        $update=DB::table('assign_branch')
                    ->where('id',$id)
                    ->update($data);
        
        Session::put('message','Branch assign update successfully done !!');
        return redirect()->back();
        
    }

    public function assignBranchEdit($id){
        // echo "string";
        // exit();
        $branchInfo=DB::table('branches')
                            ->get();
        $employeeInfo=DB::table('employees')
                            ->get();
        $assignInfo=DB::table('assign_branch as ab')
                        ->join('employees as e','e.id','ab.employeeId')
                        ->join('branches as b','b.id','ab.branchId')
                        ->select('e.id as eId','b.id as bId','ab.*')
                        ->where('ab.id',$id)
                        ->first();
        // echo "<pre/>";
        // print_r($assignInfo);
        // exit();
        
        return view("admin.branch.assignBranchEdit",compact("assignInfo","id","branchInfo","employeeInfo"));
    }

    public function branchInactive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=0;
        $inactive=DB::table('branches')
                        ->where('id',$id)
                        ->update($data);
    }

    public function branchActive(Request $request){
        $id=$request->id;
        $data=array();
        $data['status']=1;
        $inactive=DB::table('branches')
                        ->where('id',$id)
                        ->update($data);
    }

    public function branchDelete(Request $request){
        $id=$request->id;
        $delete=DB::table('branches')
                    ->where('id',$id)
                    ->delete();
    }

    public function editBranch($id){
        $singleBranchInfo=DB::table('branches')
                                ->where('id',$id)
                                ->first();
        return view("admin.branch.editBranch",compact("singleBranchInfo","id"));
    }
    public function saveBranch(Request $request){
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'addressOne' => 'required',
            'email' => 'required',
            'shortName' => 'required',
        ]);

        $data=array();
        $data['code']=$request->code;
        $data['name']=$request->name;
        $data['addressOne']=$request->addressOne;
        $data['addressSecond']=$request->addressSecond;
        $data['email']=$request->email;
        $data['status']=$request->status;
        $data['shortName']=$request->shortName;
        $data['updateDate']=date('Y-m-d');
        $order=$request->order;
        if ($order) {
            $data['order']=$request->order;
        }else{
            $data['order']=rand();
        }
        $insertBranch=DB::table('branches')
                            ->insert($data);
        if ($insertBranch) {
            Session::put('message','Branch create successfully done !!');
            return redirect()->back();
        }else{
            Session::put('messageWarning','Branch create failed !!');
            return redirect()->back();
        }
    }

    public function updateBranch(Request $request){
        $id=$request->id;
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'addressOne' => 'required',
            'email' => 'required',
            'shortName' => 'required',
        ]);

        $data=array();
        $data['code']=$request->code;
        $data['name']=$request->name;
        $data['addressOne']=$request->addressOne;
        $data['addressSecond']=$request->addressSecond;
        $data['email']=$request->email;
        $data['status']=$request->status;
        $data['shortName']=$request->shortName;
        $data['updateDate']=date('Y-m-d');
        $order=$request->order;
        if ($order) {
            $data['order']=$request->order;
        }else{
            $data['order']=rand();
        }
        $updateBranch=DB::table('branches')
                        ->where('id',$id)
                        ->update($data);
        Session::put('message','Branch update successfully done !!');
        return redirect()->back();
    }

}
