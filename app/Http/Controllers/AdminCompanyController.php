<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
date_default_timezone_set('Asia/Dhaka');
class AdminCompanyController extends Controller
{
    

    public function createCompany(){
    	
    	return view("admin.company.createCompany");
    }
    public function allCompany(){
    	$companyInfo=DB::table('companies')
    						->orderby('id','DESC')
    						->get();
        return view("admin.company.allCompany",compact("companyInfo"));
    }
    public function saveCompany(Request $request){
    	$request->validate([
		    'code' => 'required',
		    'name' => 'required',
		    'address' => 'required',
		    'emailSuffix' => 'required',
		    'shortName' => 'required',
		    'logo' => 'required',
		    'timeZone' => 'required',
		]);
		$data=array();
        $data['code']=$request->code;
        $data['name']=$request->name;
        $data['address']=$request->name;
        $data['emailSuffix']=$request->emailSuffix;
        $data['shortName']=$request->shortName;
        $data['timeZone']=$request->timeZone;
        $data['status']=$request->status;
        $data['created_at']=date('Y-m-d');
        $data['updated_at']=date('Y-m-d');
        $image=$request->file('logo');
	        if ($image) {
	            $image_name=rand();
	            $ext=strtolower($image->getClientOriginalExtension());
	            $permited  = array('jpg', 'jpeg', 'png', 'pdf');
	            if (in_array($ext, $permited) === false){
	                Session::put('messageWarning','You can only upload '.implode(', ', $permited));
	                return redirect()->back();
	            }
	            $image_full_name=$image_name.'.'.$ext;
	            $upload_path='company/img/';
	            $image_url=$upload_path.$image_full_name;
	            $success=$image->move($upload_path,$image_full_name);
	        if ($success) {
	            $data['logo']=$image_url;
	        }
	        $insertCompany=DB::table('companies')
	        					->insert($data);
	        if ($insertCompany) {
	        	Session::put('message','Company create successfully done !!');
	            return redirect()->back();
	        }else{
	        	Session::put('messageWarning','Company create failed !!');
	            return redirect()->back();
	        }
	    }
	}

	public function viewCompany($id){
		$singleCompanyInfo=DB::table('companies')
								->where('id',$id)
								->first();
		return view("admin.company.viewCompany",compact("singleCompanyInfo","id"));
	}
	public function editCompany($id){
		$singleCompanyInfo=DB::table('companies')
								->where('id',$id)
								->first();
		return view("admin.company.editCompany",compact("singleCompanyInfo","id"));
	}

	public function updateCompany(Request $request){
		$request->validate([
		    'code' => 'required',
		    'name' => 'required',
		    'address' => 'required',
		    'emailSuffix' => 'required',
		    'shortName' => 'required',
		    'timeZone' => 'required',
		]);
		$id=$request->id;
		$data=array();
        $data['code']=$request->code;
        $data['name']=$request->name;
        $data['address']=$request->name;
        $data['emailSuffix']=$request->emailSuffix;
        $data['shortName']=$request->shortName;
        $data['timeZone']=$request->timeZone;
        $data['status']=$request->status;
        $data['updated_at']=date('Y-m-d');
        // echo "<pre/>";
        // print_r($data);
        // exit();
        $image=$request->file('logo');
	        if ($image) {
	            $image_name=rand();
	            $ext=strtolower($image->getClientOriginalExtension());
	            $permited  = array('jpg', 'jpeg', 'png', 'pdf');
	            if (in_array($ext, $permited) === false){
	                Session::put('messageWarning','You can only upload '.implode(', ', $permited));
	                return redirect()->back();
	            }
	            $image_full_name=$image_name.'.'.$ext;
	            $upload_path='company/img/';
	            $image_url=$upload_path.$image_full_name;
	            $success=$image->move($upload_path,$image_full_name);
	        if ($success) {
	            $data['logo']=$image_url;
	        }
	        
	        
	    }

	    $updateCompany=DB::table('companies')
	        					->where('id',$id)
	        					->update($data);
	        
    	Session::put('message','Company update successfully done !!');
        return redirect()->back();
	}
	public function companyInactive(Request $request){
		$id=$request->id;
		$data=array();
		$data['status']=0;
		$inactive=DB::table('companies')
						->where('id',$id)
						->update($data);
	}

	public function companyActive(Request $request){
		$id=$request->id;
		$data=array();
		$data['status']=1;
		$inactive=DB::table('companies')
						->where('id',$id)
						->update($data);
	}

}
