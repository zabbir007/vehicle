
$(function (){
$(".btnUserRoleInActive").click(function(){  
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once In-Active !!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-role-inactive',
                    method: 'post',
                    data:{id:id},
                    success: function(result){
                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! Once DeActive!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    })


    $(".btnUserRoleActive").click(function(){
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once Active !!",
          icon: "success",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-role-active',
                    method: 'post',
                    data:{id:id},
                    success: function(result){


                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! Once Active!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    });
})
