
$(function (){
$(".btnUserInActive").click(function(){  
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once In-Active !!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-inactive',
                    method: 'post',
                    data:{id:id},
                    success: function(result){
                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one in-active!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    })


    $(".btnUserActive").click(function(){
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once Active !!",
          icon: "success",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-active',
                    method: 'post',
                    data:{id:id},
                    success: function(result){


                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one active!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    });

    $(".btnPermissionInActive").click(function(){  
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once In-Active !!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-permission-inactive',
                    method: 'post',
                    data:{id:id},
                    success: function(result){
                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one in-active!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    })


    $(".btnPermissionActive").click(function(){
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once Active !!",
          icon: "success",
          buttons: true,
          dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-permission-active',
                    method: 'post',
                    data:{id:id},
                    success: function(result){


                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one active!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    });

    $(".btnUserDelete").click(function(){
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once Delete !!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/user-delete',
                    method: 'post',
                    data:{id:id},
                    success: function(result){


                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one deleted!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    });
})
