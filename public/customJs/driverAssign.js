
$(function (){
    $(".btnDriverAssignDelete").click(function(){
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once Delete !!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/vehicle/assign/delete',
                    method: 'post',
                    data:{id:id},
                    success: function(result){


                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one deleted!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    });

    $(".btnBranchAssignDelete").click(function(){
         var element=$(this);
         var id = element.attr("id");
         var APP_URL = $('meta[name="_base_url"]').attr('content');
         swal({
          title: "Are you sure?",
          text: "Once Delete !!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {

                $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
                 jQuery.ajax({
                    url: APP_URL+'/admin/assign/branch/delete',
                    method: 'post',
                    data:{id:id},
                    success: function(result){


                        location.reload(true);
                    },
                      error: function() {
                        alert('Error occurs!');
                     }
                });
            swal("Poof! one deleted!", {
              icon: "success",
            });
          } else {
            swal("Your  file is safe!");
          }
        });
    });
})
