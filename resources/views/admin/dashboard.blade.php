@extends('layouts.admin')
@section('title') Dashboard @endsection
@section('content')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
	<div class="row">
	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
	                        <i class="fa fa-building font-22 avatar-title text-danger"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">5</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Total Company</p>
	                    </div>
	                </div>
	            </div> <!-- end row-->
	        </div> <!-- end widget-rounded-circle-->
	    </div> <!-- end col-->
	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
	                        <i class="fa fa-hand-o-down font-22 avatar-title text-danger"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">5</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Total Branches</p>
	                    </div>
	                </div>
	            </div> <!-- end row-->
	        </div> <!-- end widget-rounded-circle-->
	    </div> <!-- end col-->
	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
	                        <i class="fa fa-user font-22 avatar-title text-danger"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">5</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Total Employees</p>
	                    </div>
	                </div>
	            </div> <!-- end row-->
	        </div> <!-- end widget-rounded-circle-->
	    </div> <!-- end col-->
	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
	                        <i class="fa fa-car font-22 avatar-title text-danger"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">5</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Total Drivers</p>
	                    </div>
	                </div>
	            </div> <!-- end row-->
	        </div> <!-- end widget-rounded-circle-->
	    </div> <!-- end col-->
    </div>
    <div class="row">
	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
	                        <i class="fa fa-car font-22 avatar-title text-danger"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">5</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Total Vehicles</p>
	                    </div>
	                </div>
	            </div> <!-- end row-->
	        </div> <!-- end widget-rounded-circle-->
	    </div> <!-- end col-->
	</div>

	<div class="row">
		<div class="col-6">
			<div class="card">
	            <div class="card-body">
	            	<canvas id="myChart"></canvas>
	            </div>
	        </div>
	    </div>
	    <div class="col-6">
			<div class="card">
	            <div class="card-body">
	            	<canvas id="birdsChart" height="150"></canvas>
	            </div>
	        </div>
	    </div>
	</div>
	<script>
		var ctx = document.getElementById('myChart').getContext('2d');
		var chart = new Chart(ctx, {
		    // The type of chart we want to create
		    type: 'line',

		    // The data for our dataset
		    data: {
		        labels: ["January", "February", "March", "April", "May", "June", "July"],
		        datasets: [{
		            label: "Driver statistics",
		            backgroundColor: 'rgb(255, 99, 132)',
		            borderColor: 'rgb(255, 99, 132)',
		            data: [30, 40, 50, 60, 70, 80, 90],
		        }]
		    },

		    // Configuration options go here
		    options: {
		        
		    }
		});
	</script>
	<script>
		var birdsCanvas = document.getElementById("birdsChart");
		var birdsData = {
		  labels: ["January","February","March","April"],
		  datasets: [{
		    data: [1200, 1700, 800, 200],
		    backgroundColor: [
		      "rgba(255, 0, 0, 0.5)",
		      "rgba(100, 255, 0, 0.5)",
		      "rgba(200, 50, 255, 0.5)",
		      "rgba(0, 100, 255, 0.5)"
		    ]
		  }]
		};

		var polarAreaChart = new Chart(birdsCanvas, {
		  type: 'polarArea',
		  data: birdsData
		});
	</script>
@endsection