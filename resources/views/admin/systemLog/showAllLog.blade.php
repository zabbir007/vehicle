@extends('layouts.admin')
@section('title') All Log @endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col-12">
      <div class="card-box">
        <h5>All Logs</h5>
        <table class="table">
          <thead>
            <tr>
              <th scope="col" class="text-left">
                <span class="badge badge-info level">Date</span>
              </th>
              <th scope="col" class="text-center">
                <span class="badge badge-info level" style="background-color: #8A8A8A;"><i class="fa fa-fw fa-list"></i>All</span>
              </th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #B71C1C;"><i class="fa fa-fw fa-bug"></i> Emergency </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #D32F2F;"> <i class="fa fa-fw fa-bullhorn"></i> Alert </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #F44336;"> <i class="fa fa-fw fa-heartbeat"></i> Critical </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #FF5722;"> <i class="fa fa-fw fa-times-circle"></i> Error </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #FF9100;"> <i class="fa fa-fw fa-exclamation-triangle"></i> Warning </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #4CAF50;"> <i class="fa fa-fw fa-exclamation-circle"></i> Notice </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #1976D2;"> <i class="fa fa-fw fa-info-circle"></i> Info </span></th>
              <th scope="col" class="text-center"><span class="badge badge-info level" style="background-color: #90CAF9;"> <i class="fa fa-fw fa-life-ring"></i> Debug </span></th>
              <th scope="col" class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>
                <a href="#" class="btn btn-sm btn-primary"> 2020-10-12 </a>
              </th>
              <td><span class="badge badge-info level">1</span></td>
              <td><span class="badge badge-info level">1</span></td>
              <td><span class="badge badge-info level">1</span></td>
              <th><span class="badge badge-info level">1</span></th>
              <td><span class="badge badge-info level">1</span></td>
              <td><span class="badge badge-info level">1</span></td>
              <td><span class="badge badge-info level">1</span></td>
              <th><span class="badge badge-info level">1</span></th>
              <td><span class="badge badge-info level">1</span></td>
              <td>
                <a href="#" class="btn btn-sm btn-info "><i class="fa fa-search"></i> </a>
                <a href="#" class="btn btn-sm btn-success ml-0"> <i class="fa fa-download"></i> </a>
                <button type="button" class="btn btn-sm btn-danger mt-1"><i class="fas fa-trash"></i></button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection