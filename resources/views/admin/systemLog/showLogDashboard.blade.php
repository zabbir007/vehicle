@extends('layouts.admin')
@section('title') Log Dashboard @endsection
@section('content')
<link href="{{asset('custom_css/custom.css')}}" rel="stylesheet" type="text/css" />
<div class="row">
    <div class="col-5">
    	<div id="piechart" ></div>
    </div>
    <div class="col-7">
    	<div class="row">
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-list avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>All</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">99 entries - 100%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-bug avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Emergency</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">0 entries - 0%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-bullhorn avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Alert</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">99 entries - 100%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-heartbeat avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Critical</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">0 entries - 0%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-times-circle avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Error</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">99 entries - 100%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-exclamation-triangle avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Warning</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">0 entries - 0%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-exclamation-circle avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Notice</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">99 entries - 100%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-info-circle avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Info</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">0 entries - 0%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>

    	<div class="row">
    		<div class="col-6">
    			<div class="card-box">
		            <div class="row">
		                <div class="col-6">
		                    <div class="avatar-sm bg-blue rounded">
		                        <i class="fa fa-fw fa-life-ring avatar-title font-22 text-white"></i>
		                    </div>
		                </div>
		                <div class="col-6">
		                    <div class="text-right">
		                        <h3 class="text-dark my-1"><span>Debug</span></h3>
		                    </div>
		                </div>
		            </div>
		            <div class="mt-3">
		                <h6 class="text-uppercase">99 entries - 100%</h6>
		                <div class="progress progress-sm m-0">
		                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
		                        <span class="sr-only">60% Complete</span>
		                    </div>
		                </div>
		            </div>
		        </div>
    		</div>
    	</div>
		    	
    </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Emergency', 8],
  ['Critical', 2],
  ['Alert', 4],
  ['Error', 2],
  ['Warning', 2],
  ['Notice', 2],
  ['Info', 2],
  ['Debug', 2]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Log Dashboard', 'width':430, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>
@endsection
