@extends('layouts.admin')
@section('title') Create Employee @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Employee Management</span>
                        <span>Add New Employee</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	<a href="{{route('createEmployee')}}">
                    		<button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Employee
	                        </button>	
                    	</a>
                        <a href="{{route('allEmployee')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-th-list"></i></span>All Employees
	                        </button>
	                    </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <form action="{{route('saveEmployee')}}"  class="parsley-examples" method="post" novalidate>
                @csrf
	                @if ($errors->any())
					    <div class="alert alert-danger" id="alertShow">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
	                <?php 
	                    $message=Session::get('message');
	                    if($message){
	                ?>
	                    <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                        <?php
	                            echo $message;
	                            Session::put('message','');
	                        ?>
	                    </div>
	                <?php
	                	}
	                ?>

	                <?php 
	                    $messageWarning=Session::get('messageWarning');
	                    if($messageWarning){
	                ?>
	                    <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                        <?php
	                            echo $messageWarning;
	                            Session::put('messageWarning','');
	                        ?>
	                    </div>
	                <?php
	                	}
	                ?>
	                <div class="row">
	                	<div class="col-6">
	                		<div class="form-group mb-3">
			                    <label for="validationCustom03">Company ID</label>
			                    <select id="heard" class="form-control" required="" name="companyId" data-toggle="select2">
			                    	<option value="">Select</option>
		                            <option value="1">Transcom Distribution Company Limited</option>
		                        </select>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">First Name</label>
			                    <input type="text" name="firstName" class="form-control" id="validationCustom03" placeholder="Employee first name" required>
			                    <div class="invalid-feedback">
			                        Please provide a first name.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Designation</label>
			                    <select id="heard" class="form-control" name="designation" required="" data-toggle="select2">
			                    	<option value="">Select</option>
		                            <option value="1">Jr.System Analyst</option>
		                            <option value="2">NDM</option>
		                        </select>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Email</label>
			                    <input type="text" name="email" class="form-control" id="validationCustom03" placeholder="Employee email" required>
			                    <div class="invalid-feedback">
			                        Please provide a email.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Active</label>
			                    <input id="checkbox2" value="1" name="status"  type="checkbox" checked>
			                </div>
			                
	                	</div>
	                	
	                	<div class="col-6">
	                		<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Employee Id</label>
			                    <input type="text" class="form-control" name="employeeId" id="validationCustom03" placeholder="Employee Code" required>
			                    <div class="invalid-feedback">
			                        Please provide a employee code.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Last Name</label>
			                    <input type="text" name="lastName" class="form-control" id="validationCustom03" placeholder="Employee last name" required>
			                    <div class="invalid-feedback">
			                        Please provide a last name.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Department</label>
			                    <input type="text" placeholder="Department" name="department" class="form-control" id="validationCustom03" required>
			                    <div class="invalid-feedback">
			                        Please provide a department.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Mobile</label>
			                    <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57 event.charCode == 43'  placeholder="Mobile Number" name="mobile" class="form-control" id="validationCustom03" required>
			                    <div class="invalid-feedback">
			                        Please provide a mobile number.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Order</label>
			                    <input type="text" name="order" placeholder="Enter next order number, keep blank for auto" class="form-control" id="validationCustom03">
			                    
			                </div>
	                	</div>
	                	
	                </div>
	                <hr style="border-top: 1px dashed black;">
	                <div class="row">
	                    <div class="col-6">
	                        <div class="form-group mb-3">
			                    <input id="checkbox2" value="1" name="loginStatus"  type="checkbox">
			                    <span style="color: black;font-size: 110%"> Create Login User</span>
			                </div>
	                        
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-6">
	                    	<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Username</label>
			                    <input type="text" placeholder="Username" name="userName" class="form-control" id="validationCustom03" >
			                    
			                </div>
	                    </div>
	                    <div class="col-6">
	                    	<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Password</label>
			                    <input type="text" placeholder="Password" name="password" class="form-control" id="validationCustom03">
			                    
			                </div>
	                    </div>
	                </div>
	            	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
	            </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 5000);
</script>
@endsection