@extends('layouts.admin')
@section('title') All Employee @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Employee Management</span>
                        <span>All Active Employees</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createEmployee')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Employee
                            </button>   
                        </a>
                        <a href="{{route('allEmployee')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Employees
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Branches</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employeeInfo as $employee)
                        <tr>	
                            <td>{{$employee->employeeId}}</td>
                            <td>{{$employee->firstName.' '.$employee->lastName}}</td>
                            <td>
                                <?php
                                    if ($employee->designation==1) {
                                        echo "jr.system analyst";
                                    }else{
                                        echo "NDM";
                                    }
                                ?>
                            </td>
                            <td>{{$employee->branch}}</td>
                            <td>{{$employee->updateDate}}</td>
                            <td>
                                <?php
                                    if ($employee->status==1) {
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-success">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('viewEmployee',[$employee->id])}}" title="View Employee" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editEmployee',[$employee->id])}}" title="Update Employee" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="javascript:void(0);" title="Trash Employee" id="{{$employee->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnEmployeeDelete"> <i class="fas fa-trash"></i></a>
                                <?php
                                if($employee->status=='1'){
                                ?>
                                    <a href="javascript:void(0);" id="{{$employee->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnEmployeeInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }else{
                                ?>
                                <a href="javascript:void(0);" id="{{$employee->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnEmployeeActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                                </td>
                                <?php
                                    }
                                ?>
                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection