@extends('layouts.admin')
@section('title') Assign Vehicle @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Vehicle Driver Manager</span>
                        <span>Assign Branch Vehicle Drivers</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	<a href="{{route('assignVehicleCreate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Assign Driver
                            </button>
                        </a>
                        <a href="{{route('assignVehicle')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Records
                            </button>
                        </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive">
                    <thead>
                        <tr>
                            <th>Vehicle</th>
                            <th>Branch</th>
                            <th>Driver</th>
                            <th>Take Over Date</th>
                            <th>Hand Over Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($assignInfo as $assign)
                        <tr>
                            <td>{{$assign->vName}}</td>
                            <td>{{$assign->bName}}</td>
                            <td>{{$assign->dName}}</td>
                            <td>{{$assign->takeDate}}</td>
                            <td>{{$assign->overDate}}</td>
                            <td>
                                <?php
                                    if($assign->status=='1'){
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-danger">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('assignVehicleView',[$assign->id])}}" title="View Company" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('assignVehicleEdit',[$assign->id])}}" title="Update Company" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="javascript:void(0);" title="Trash Driver" id="{{$assign->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnDriverAssignDelete"> <i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection