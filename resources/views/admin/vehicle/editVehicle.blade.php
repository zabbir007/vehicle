@extends('layouts.admin')
@section('title') Create Vehicle @endsection
@section('content')
	<div class="card-box">
		<div class="row">
            <div class="col-6">
                <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                <span style="color: black;font-size: 130%"> Vehicle Management</span>
                <span>Add New Vehicle</span>
            </div>
            <div class="col-6" style="text-align: left;">
            	<a href="{{route('viewVehicle',[$id])}}">
	                <button type="button" class="btn btn-success waves-effect waves-light">
	                    <span class="btn-label"><i class="fas fa-plus-circle"></i></span>View
	                </button>
                </a>
            	<a href="{{route('createVehicle')}}">
	                <button type="button" class="btn btn-success waves-effect waves-light">
	                    <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Vehicle
	                </button>
                </a>
                <a href="{{route('allVehicle')}}">
	                <button type="button" class="btn btn-success waves-effect waves-light">
	                    <span class="btn-label"><i class="fas fa-th-list"></i></span>All Vehicles
	                </button>
	            </a>
            </div>
        </div>
        <hr style="border-top: 1px dashed black;">
	    <ul class="nav nav-tabs">
	        <li class="nav-item">
	            <a href="#home" data-toggle="tab" aria-expanded="true" class="nav-link active">
	                Vehicle Info
	            </a>
	        </li>
	        <li class="nav-item">
	            <a href="#profile" data-toggle="tab" aria-expanded="false" class="nav-link">
	                Documents
	            </a>
	        </li>
	        <li class="nav-item">
	            <a href="#fuel" data-toggle="tab" aria-expanded="false" class="nav-link">
	                Fuels
	            </a>
	        </li>
	    </ul>
	    <form action="{{route('updateVehicle')}}"  class="parsley-examples" method="post" novalidate>
        	@csrf
            @if ($errors->any())
			    <div class="alert alert-danger" id="alertShow">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
            <?php 
                $message=Session::get('message');
                if($message){
            ?>
                <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $message;
                        Session::put('message','');
                    ?>
                </div>
            <?php
            	}
            ?>

            <?php 
                $messageWarning=Session::get('messageWarning');
                if($messageWarning){
            ?>
                <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php
                        echo $messageWarning;
                        Session::put('messageWarning','');
                    ?>
                </div>
            <?php
            	}
            ?>
	    	<div class="tab-content">
		        <div class="tab-pane show active" id="home">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Company ID</label>
			                    <select id="heard" class="form-control" required="" name="companyId" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->companyId=='1'){echo "selected";} ?> >Transcom Distribution Company Limited</option>
		                        </select>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Name</label>
			                    <input type="text" class="form-control" value="{{$singleVehicleInfo->name}}" name="name" id="validationCustom03" placeholder="Vehicle Name" required>
			                    <div class="invalid-feedback">
			                        Please provide a vehicle name.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Manf.Year</label>
			                    <input type="text" name="manfYear" value="{{$singleVehicleInfo->manfYear}}" class="form-control" id="validationCustom03" placeholder="Manufacturing Year" required>
			                    <div class="invalid-feedback">
			                        Please provide a manufacturing year.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Weight</label>
			                    <input type="text" name="weight" value="{{$singleVehicleInfo->weight}}" class="form-control" id="validationCustom03" placeholder="weight" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">License No.</label>
			                    <input type="text" name="licenseNo" value="{{$singleVehicleInfo->licenseNo}}" class="form-control" id="validationCustom03" placeholder="License Number" required>
			                    <div class="invalid-feedback">
			                        Please provide a License Number.
			                    </div>
			                </div>
			                <input type="hidden" name="id" value="{{$singleVehicleInfo->id}}">
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Chassis No.</label>
			                    <input type="text" name="chassisNo" value="{{$singleVehicleInfo->chassisNo}}" class="form-control" id="validationCustom03" placeholder="Chassis Number" required>
			                    <div class="invalid-feedback">
			                        Please provide a chassis number.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">VIN No.</label>
			                    <input type="text" name="vinNo" value="{{$singleVehicleInfo->vinNo}}" class="form-control" id="validationCustom03" placeholder="Vehicle Identification No" required>
			                    <div class="invalid-feedback">
			                        Please provide a VIN No.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Opening Millage</label>
			                    <input type="text" name="opeMill" value="{{$singleVehicleInfo->opeMill}}" class="form-control" id="validationCustom03" placeholder="Opening Millage" required>
			                    <div class="invalid-feedback">
			                        Please provide a Opening Millage.
			                    </div>
			                </div>
			               
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Active</label>
			                    <input id="checkbox2" value="1" <?php if($singleVehicleInfo->status=='1'){echo "checked";} ?> name="status" type="checkbox" checked>
			                </div>
		        		</div>
		        		<div class="col-6">
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Vehicle Type</label>
			                    <select id="heard" class="form-control" name="vehicleType" required="" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->vehicleType=='1'){echo "selected";} ?> >Car</option>
		                            <option value="2" <?php if($singleVehicleInfo->vehicleType=='2'){echo "selected";} ?> >Truck</option>
		                        </select>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Manufacturer</label>
			                    <input type="text" name="manufacture" value="{{$singleVehicleInfo->manufacture}}" class="form-control" id="validationCustom03" placeholder="Manufacturer" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Model</label>
			                    <input type="text" name="model" value="{{$singleVehicleInfo->model}}" class="form-control" id="validationCustom03" placeholder="Model" required>
			                    <div class="invalid-feedback">
			                        Please provide a model.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Lifetime</label>
			                    <input type="text" name="lifeTime" value="{{$singleVehicleInfo->lifeTime}}" class="form-control" id="validationCustom03" placeholder="Lifetime" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">License Year</label>
			                    <input type="text" name="licenseYear" value="{{$singleVehicleInfo->licenseYear}}" class="form-control" id="validationCustom03" placeholder="License Year">
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Engine No.</label>
			                    <input type="text" name="engineNo" value="{{$singleVehicleInfo->engineNo}}" class="form-control" id="validationCustom03" placeholder="Engine Number" required>
			                    <div class="invalid-feedback">
			                        Please provide a Engine Number.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Purchase Date</label>
			                    <input type="text" name="purDate" value="{{$singleVehicleInfo->purDate}}" class="form-control" id="validationCustom03" placeholder="Purchase Date" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Order</label>
			                    <input type="text" name="order" value="{{$singleVehicleInfo->order}}" class="form-control" id="validationCustom03" placeholder="Enter next order number, keep blank for auto">
			                    
			                </div>

		        		</div>
		        	</div>            
		        </div>
		        <div class="tab-pane" id="profile">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Type</label>
			                    <select id="heard" name="fitDocumentType" class="form-control" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->fitDocumentType=='1'){echo "selected";} ?> >Fitness Paper</option>
		                            <option value="2" <?php if($singleVehicleInfo->fitDocumentType=='2'){echo "selected";} ?> >Insurance Paper</option>
		                            <option value="3" <?php if($singleVehicleInfo->fitDocumentType=='3'){echo "selected";} ?> >Tax Token</option>
		                        </select>
			                </div>
				        	<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Number</label>
			                    <input type="text" value="{{$singleVehicleInfo->fitDocumentNum}}" name="fitDocumentNum" class="form-control" id="validationCustom03" placeholder="Document Number" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Issue Date</label>
			                    <input type="date" name="fitIssue" value="{{$singleVehicleInfo->fitIssue}}" class="form-control" id="validationCustom03" placeholder="Issue date">
			                    
			                </div>
		        		</div>
		        		<div class="col-6">
		        			<div class="form-group mt-6">
			                    <label for="validationCustom03">Issuing Authority</label>
			                    <input type="text" name="fitIssuing" value="{{$singleVehicleInfo->fitIssuing}}" class="form-control" id="validationCustom03" placeholder="Issuing authority">
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Expiry Date</label>
			                    <input type="date" name="fitExpire" value="{{$singleVehicleInfo->fitExpire}}" class="form-control" id="validationCustom03" placeholder="Expiry date">
			                    
			                </div>
		        		</div>
		        	</div>
		        	<hr style="border-top: 1px dashed black;">
		        	<hr style="border-top: 1px dashed black;">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Type</label>
			                    <select id="heard" name="insDocumentType" class="form-control" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->insDocumentType=='1'){echo "selected";} ?> >Fitness Paper</option>
		                            <option value="2" <?php if($singleVehicleInfo->insDocumentType=='2'){echo "selected";} ?> >Insurance Paper</option>
		                            <option value="3" <?php if($singleVehicleInfo->insDocumentType=='3'){echo "selected";} ?> >Tax Token</option>
		                        </select>
			                </div>
				        	<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Number</label>
			                    <input type="text" name="insDocumentNum" value="{{$singleVehicleInfo->insDocumentNum}}" class="form-control" id="validationCustom03" placeholder="Document Number" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Issue Date</label>
			                    <input type="date" name="insIssue" value="{{$singleVehicleInfo->insIssue}}" class="form-control" id="validationCustom03" placeholder="Issue date">
			                    
			                </div>
		        		</div>
		        		<div class="col-6">
		        			<div class="form-group mt-6">
			                    <label for="validationCustom03">Issuing Authority</label>
			                    <input type="text" name="insIssuing" value="{{$singleVehicleInfo->insIssuing}}" class="form-control" id="validationCustom03" placeholder="Issuing authority">
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Expiry Date</label>
			                    <input type="date" name="insExpire" value="{{$singleVehicleInfo->insExpire}}" class="form-control" id="validationCustom03" placeholder="Expiry date">
			                    
			                </div>
		        		</div>
		        	</div>
		        	<hr style="border-top: 1px dashed black;">
		        	<hr style="border-top: 1px dashed black;">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Type</label>
			                    <select id="heard" name="taxDocumentType" class="form-control" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->taxDocumentType=='1'){echo "selected";} ?> >Fitness Paper</option>
		                            <option value="2" <?php if($singleVehicleInfo->taxDocumentType=='2'){echo "selected";} ?> >Insurance Paper</option>
		                            <option value="3" <?php if($singleVehicleInfo->taxDocumentType=='3'){echo "selected";} ?> >Tax Token</option>
		                        </select>
			                </div>
				        	<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Number</label>
			                    <input type="text" name="taxDocumentNum" value="{{$singleVehicleInfo->taxDocumentNum}}" class="form-control" id="validationCustom03" placeholder="Document Number" >
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Issue Date</label>
			                    <input type="date" name="taxIssue" value="{{$singleVehicleInfo->taxIssue}}" class="form-control" id="validationCustom03" placeholder="Issue date">
			                    
			                </div>
		        		</div>
		        		<div class="col-6">
		        			<div class="form-group mt-6">
			                    <label for="validationCustom03">Issuing Authority</label>
			                    <input type="text" name="taxIssuing" value="{{$singleVehicleInfo->taxIssuing}}" class="form-control" id="validationCustom03" placeholder="Issuing authority">
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Expiry Date</label>
			                    <input type="date" name="taxExpire" value="{{$singleVehicleInfo->taxExpire}}" class="form-control" id="validationCustom03" placeholder="Expiry date">
			                    
			                </div>
		        		</div>
		        	</div>
		        	<hr style="border-top: 1px dashed black;">
		        </div>
		        <div class="tab-pane" id="fuel">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Entry Date</label>
			                    <input type="date" class="form-control" value="{{$singleVehicleInfo->fuelEntryDate}}" name="fuelEntryDate" id="validationCustom03" placeholder="Entry date">
			                    
			                </div>
				        	<div class="form-group mb-3">
			                    <label for="validationCustom03">Dual Tank</label>
			                    <input id="checkbox2" name="dualTank" <?php if($singleVehicleInfo->dualTank=='1'){echo "checked";} ?> value="1" type="checkbox">
			                </div>
		        		</div>
		        	</div>
		        	<hr style="border-top: 1px dashed black;">
		        	<hr style="border-top: 1px dashed black;">
		        	<div class="alert alert-primary" role="alert">
					  Main Fuel Tank
					</div>
					<div class="row">
						<div class="col-4">
							<div class="form-group mb-1">
			                    <label for="validationCustom03">Fuel</label>
			                    <select id="heard" class="form-control" name="firstFuel" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->firstFuel=='1'){echo "selected";} ?> >Deisel</option>
		                            <option value="2" <?php if($singleVehicleInfo->firstFuel=='2'){echo "selected";} ?> >Petrol</option>
		                            <option value="3" <?php if($singleVehicleInfo->firstFuel=='3'){echo "selected";} ?> >Octane</option>
		                            <option value="4" <?php if($singleVehicleInfo->firstFuel=='4'){echo "selected";} ?> >Cng</option>
		                        </select>
			                </div>
						</div>
						<div class="col-4">
							<div class="form-group mb-1">
			                    <label for="validationCustom03">Capacity</label>
			                    <input type="text" value="{{$singleVehicleInfo->firstCapacity}}" name="firstCapacity" class="form-control" id="validationCustom03" placeholder="Main tank fuel capacity">
			                    
			                </div>
						</div>
						<div class="col-4">
							<div class="form-group mb-1">
			                    <label for="validationCustom03">Standard Millage</label>
			                    <input type="text" name="firstStandard" value="{{$singleVehicleInfo->firstStandard}}" class="form-control" id="validationCustom03" placeholder="Millage for per kilo.">
			                    
			                </div>
						</div>
					</div>
					<hr style="border-top: 1px dashed black;">
		        	<hr style="border-top: 1px dashed black;">
		        	<div class="alert alert-primary" role="alert">
					  Main Fuel Tank
					</div>
					<div class="row">
						<div class="col-4">
							<div class="form-group mb-1">
			                    <label for="validationCustom03">Fuel</label>
			                    <select id="heard" class="form-control" name="secondFuel" data-toggle="select2">
		                            <option value="1" <?php if($singleVehicleInfo->secondFuel=='1'){echo "selected";} ?> >Deisel</option>
		                            <option value="2" <?php if($singleVehicleInfo->secondFuel=='2'){echo "selected";} ?> >Petrol</option>
		                            <option value="3" <?php if($singleVehicleInfo->secondFuel=='3'){echo "selected";} ?> >Octane</option>
		                            <option value="4" <?php if($singleVehicleInfo->secondFuel=='4'){echo "selected";} ?> >Cng</option>
		                        </select>
			                </div>
						</div>
						<div class="col-4">
							<div class="form-group mb-1">
			                    <label for="validationCustom03">Capacity</label>
			                    <input type="text" name="secondCapacity" value="{{$singleVehicleInfo->secondCapacity}}" class="form-control" id="validationCustom03" placeholder="Main tank fuel capacity">
			                    
			                </div>
						</div>
						<div class="col-4">
							<div class="form-group mb-1">
			                    <label for="validationCustom03">Standard Millage</label>
			                    <input type="text" name="secondStandard" value="{{$singleVehicleInfo->secondStandard}}" class="form-control" id="validationCustom03" placeholder="Millage for per kilo.">
			                    
			                </div>
						</div>
					</div>
		        </div>
		        <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Update Changes</button>
	    	</div>
	    </form>
	</div> <!-- end card-box-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 5000);
</script>
@endsection