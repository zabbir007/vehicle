@extends('layouts.admin')
@section('title') Vehicle Driver @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Vehicle Driver Manager</span>
                        <span>Assign Branch Vehicle Drivers</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('vehicleDriverCreate')}}">
                        <button type="button" class="btn btn-success waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Assign Driver
                        </button>
                        </a>
                        <button type="button" class="btn btn-success waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-th-list"></i></span>All Records
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <div class="row">
                	<div class="col-6">
                		<div class="form-group mb-3">
		                    <label for="validationCustom03">Branch</label>
		                    <select id="heard" class="form-control" required="" data-toggle="select2">
	                            <option value="0">select</option>
	                            <option value="1">Corporate</option>
	                        </select>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Vehicle</label>
		                    <select id="heard" class="form-control" required="" data-toggle="select2">
	                            <option value="0">select</option>
	                            <option value="1">Semi Truck (DHAKA-METRO-HA-244855)</option>
	                        </select>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Driver</label>
		                    <select id="heard" class="form-control" required="" data-toggle="select2">
	                            <option value="0">select</option>
	                            <option value="1">Hasan</option>
	                        </select>
		                </div>
                		<div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Take Over Date</label>
		                    <input type="text" class="form-control" id="validationCustom03" placeholder="Date of take over" required>
		                    <div class="invalid-feedback">
		                        Please provide a Take Over Date.
		                    </div>
		                </div>
                	</div>
                	
                	<div class="col-6">
                		<div class="form-group " style="margin-top: 54%;">
		                    <label for="validationCustom03 font-weight-bold">Hand Over Date</label>
		                    <input type="text" class="form-control" id="validationCustom03" placeholder="Date of hand over">
		                </div>
                	</div>
                	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection