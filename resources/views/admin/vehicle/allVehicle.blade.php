@extends('layouts.admin')
@section('title') All Vehicle @endsection
@section('content')
<style type="text/css">
    body {
  --table-width: 100%; /* Or any value, this will change dinamically */
}
tbody {
  display:block;
  max-height:500px;
  overflow-y:auto;
}
thead, tbody tr {
  display:table;
  width: var(--table-width);
  table-layout:fixed;
}
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Vehicle Management</span>
                        <span>All Active Vehicles</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createVehicle')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Vehicle
                            </button>
                        </a>
                        <a href="{{route('allVehicle')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Vehicles
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table-responsive">
                    <thead>
                        <tr>
                            <th>Vehicle Name</th>
                            <th>Vehicle Type</th>
                            <th>Manufacturer</th>
                            <th>Manf. Year</th>
                            <th>License No</th>
                            <th>License Year</th>
                            <th>Chassis No</th>
                            <th>Engine No</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($vehicleInfo as $vehicle)
                        <tr>
                            <td>{{$vehicle->name}}</td>
                            <td>
                                <?php
                                    if ($vehicle->vehicleType==1) {
                                        echo "Car";
                                    }else{
                                        echo "Truck";
                                    }
                                ?>
                            </td>
                            <td>{{$vehicle->manufacture}}</td>
                            <td>{{$vehicle->manfYear}}</td>
                            <td>{{$vehicle->licenseNo}}</td>
                            <td>{{$vehicle->licenseYear}}</td>
                            <td>{{$vehicle->chassisNo}}</td>
                            <td>{{$vehicle->engineNo}}</td>
                            <td>
                                <?php
                                    if ($vehicle->status==1) {
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-success">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('viewVehicle',[$vehicle->id])}}" title="View Employee" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editVehicle',[$vehicle->id])}}" title="Update Employee" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="javascript:void(0);" title="Trash Employee" id="{{$vehicle->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnVehicleDelete"> <i class="fas fa-trash"></i></a>
                                <?php
                                if($vehicle->status=='1'){
                                ?>
                                    <a href="javascript:void(0);" id="{{$vehicle->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnVehicleInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }else{
                                ?>
                                <a href="javascript:void(0);" id="{{$vehicle->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnVehicleActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection