@extends('layouts.admin')
@section('title') All Driver @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Drivers Management</span>
                        <span>All Active Drivers</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('driverCreate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Driver
                            </button>
                        </a>
                        <a href="{{route('allDriver')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Drivers
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Driver ID</th>
                            <th>Name</th>
                            <th>Branches</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($driverInfo as $driver)
                        <tr>
                            <td>{{$driver->driverId}}</td>
                            <td>{{$driver->name}}</td>
                            <td></td>
                            <td>{{$driver->updateDate}}</td>
                            <td>
                                <?php
                                    if($driver->status=='1'){
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-danger">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('viewDriver',[$driver->id])}}" title="View Driver" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editDriver',[$driver->id])}}" title="Update Driver" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="javascript:void(0);" title="Trash Driver" id="{{$driver->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnDriverDelete"> <i class="fas fa-trash"></i></a>
                                <?php
                                if($driver->status=='1'){
                                ?>
                                    <a href="javascript:void(0);" id="{{$driver->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnDriverInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }else{
                                ?>
                                <a href="javascript:void(0);" id="{{$driver->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnDriverActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                                </td>
                                <?php
                                    }
                                ?>
                                 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection