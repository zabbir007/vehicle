@extends('layouts.admin')
@section('title') Create Driver @endsection
@section('content')
	<div class="card-box">
		<div class="row">
            <div class="col-6">
                <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                <span style="color: black;font-size: 130%"> Driver Management</span>
                <span>Add New Driver</span>
            </div>
            <div class="col-6" style="text-align: left;">
            	<a href="{{route('driverCreate')}}">
	                <button type="button" class="btn btn-success waves-effect waves-light">
	                    <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Driver
	                </button>
	            </a>
	            <a href="{{route('allDriver')}}">
	                <button type="button" class="btn btn-success waves-effect waves-light">
	                    <span class="btn-label"><i class="fas fa-th-list"></i></span>All Drivers
	                </button>
	            </a>
                <button type="button" class="btn btn-primary waves-effect waves-light">
                    <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                </button>
            </div>
        </div>
        <hr style="border-top: 1px dashed black;">
	    <ul class="nav nav-tabs">
	        <li class="nav-item">
	            <a href="#home" data-toggle="tab" aria-expanded="true" class="nav-link active">
	                Driver Info
	            </a>
	        </li>
	        <li class="nav-item">
	            <a href="#profile" data-toggle="tab" aria-expanded="false" class="nav-link">
	                Documents
	            </a>
	        </li>
	    </ul>
	    <form action="{{route('saveDriver')}}"  class="parsley-examples" method="post" enctype="multipart/form-data" novalidate>
                @csrf
	                @if ($errors->any())
					    <div class="alert alert-danger" id="alertShow">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
	                <?php 
	                    $message=Session::get('message');
	                    if($message){
	                ?>
	                    <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                        <?php
	                            echo $message;
	                            Session::put('message','');
	                        ?>
	                    </div>
	                <?php
	                	}
	                ?>

	                <?php 
	                    $messageWarning=Session::get('messageWarning');
	                    if($messageWarning){
	                ?>
	                    <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                            <span aria-hidden="true">&times;</span>
	                        </button>
	                        <?php
	                            echo $messageWarning;
	                            Session::put('messageWarning','');
	                        ?>
	                    </div>
	                <?php
	                	}
	                ?>
	    	<div class="tab-content">
		        <div class="tab-pane show active" id="home">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Name</label>
			                    <input type="text" class="form-control" name="name" id="validationCustom03" placeholder="Driver Name" required>
			                    <div class="invalid-feedback">
			                        Please provide a name.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Date Of Birth</label>
			                    <input type="date" class="form-control" id="validationCustom03" placeholder="Date Of Birth" name="birth" required>
			                    <div class="invalid-feedback">
			                        Please provide a date of birth.
			                    </div>
			                </div>	
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Blood Group</label>
			                    <input type="text" class="form-control" name="blood" id="validationCustom03" placeholder="Blood Group">
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Present Address</label>
			                    <textarea required class="form-control" name="presentAddress" id="validationCustom03"></textarea>
			                    <div class="invalid-feedback">
			                        Please provide a Present Address.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Photo</label>
			                    <input type="file" class="dropify" name="photo" data-default-file="" />
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Active</label>
			                    <input id="checkbox2" name="status" value="1" type="checkbox" checked>
			                </div>
		        		</div>
		        		<div class="col-6">
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Driver Id</label>
			                    <input type="text" name="driverId" class="form-control" id="validationCustom03" placeholder="Driver Id" required>
			                    <div class="invalid-feedback">
			                        Please provide Driver Id.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Mobile</label>
			                    <input type="number" name="mobile" class="form-control" id="validationCustom03" placeholder="Mobile Number" required>
			                    <div class="invalid-feedback">
			                        Please provide mobile number.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Date Of Joining</label>
			                    <input type="date" name="joining" class="form-control" id="validationCustom03" placeholder="Date Of joining" required>
			                    <div class="invalid-feedback">
			                        Please provide a date of joining.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Emergency Mobile</label>
			                    <input type="number" name="emMobile" class="form-control" id="validationCustom03" placeholder="Emergency mobile number">
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Permanent Address</label>
			                    <textarea required class="form-control" name="permanentAddress" id="validationCustom03"></textarea>
			                    <div class="invalid-feedback">
			                        Please provide a Permanent Address.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Order</label>
			                    <input type="text" class="form-control" name="orderNumber" id="validationCustom03" placeholder="Enter next order number, keep blank for auto">
			                </div>
		        		</div>
		        	</div>            
		        </div>
		        <div class="tab-pane" id="profile">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Type</label>
			                    <select id="heard" class="form-control" name="nidDocument" data-toggle="select2">
		                            <option value="1">NID</option>
		                            <option value="2">Driving License</option>
		                        </select>
			                </div>
				        	<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Number</label>
			                    <input type="text" class="form-control" name="nidNumber" id="validationCustom03" placeholder="Document Number" >
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Issue Date</label>
			                    <input type="date" class="form-control" name="nidIssue" id="validationCustom03" placeholder="Issue date">
			                </div>
		        		</div>
		        		<div class="col-6">
		        			<div class="form-group mt-6">
			                    <label for="validationCustom03">Issuing Authority</label>
			                    <input type="text" class="form-control" name="nidIssuing" id="validationCustom03" placeholder="Issuing authority">
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Expiry Date</label>
			                    <input type="date" class="form-control" name="nidExpire" id="validationCustom03" placeholder="Expiry date">
			                </div>
		        		</div>
		        	</div>
		        	<hr style="border-top: 1px dashed black;">
		        	<div class="row">
		        		<div class="col-6">
		        			<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Type</label>
			                    <select id="heard" class="form-control" name="drivingDocument" data-toggle="select2">
		                            <option value="1">NID</option>
		                            <option value="2">Driving License</option>
		                        </select>
			                </div>
				        	<div class="form-group mb-3">
			                    <label for="validationCustom03">Document Number</label>
			                    <input type="text" class="form-control" name="drivingNumber" id="validationCustom03" placeholder="Document Number">
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Issue Date</label>
			                    <input type="date" class="form-control" name="drivingIssue" id="validationCustom03" placeholder="Issue date">
			                    
			                </div>
		        		</div>
		        		<div class="col-6">
		        			<div class="form-group mt-6">
			                    <label for="validationCustom03">Issuing Authority</label>
			                    <input type="text" class="form-control" name="drivingIssuing" id="validationCustom03" placeholder="Issuing authority">
			                    
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Expiry Date</label>
			                    <input type="date" class="form-control" name="drivingExpire" id="validationCustom03" placeholder="Expiry date">
			                    
			                </div>
		        		</div>
		        	</div>
		        	<hr style="border-top: 1px dashed black;">
		        </div>
		        <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
	    	</div>
	    </form>
	</div> <!-- end card-box-->
@endsection