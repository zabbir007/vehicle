@extends('layouts.admin')
@section('title') Create Fuel @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Fuel Rate Manager</span>
                        <span>Add New Fuel Rate</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createFuelRate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Fuel Rate
                            </button>
                        </a>
                        <a href="{{route('allFuelRate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Fuel Rate
                            </button>
                        </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <form action="{{route('saveFuelRate')}}"  class="parsley-examples" method="post" novalidate>
                @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger" id="alertShow">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <?php 
                        $message=Session::get('message');
                        if($message){
                    ?>
                        <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                    <?php
                        }
                    ?>

                    <?php 
                        $messageWarning=Session::get('messageWarning');
                        if($messageWarning){
                    ?>
                        <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                    <?php
                        }
                    ?>
                    <div class="row">
                    	<div class="col-6">
                    		<div class="form-group mb-3">
    		                    <label for="validationCustom03">Branch</label>
    		                    <select id="heard" required name="branchName" class="form-control" required="" data-toggle="select2">
    	                            <option value="1">Corporate</option>
    	                            <option value="2">Motijheel</option>
    	                        </select>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03 font-weight-bold">Entry Date</label>
    		                    <input type="date" class="form-control" name="entryDate" id="validationCustom03" placeholder="Entry Date" value="<?php echo date('Y-m-d'); ?>" required>
    		                    <div class="invalid-feedback">
    		                        Please provide a entry date.
    		                    </div>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">Deisel Rate</label>
    		                    <div class="input-group mb-3">
                                    <input type="text" required name="deiselRate" class="form-control" id="inlineFormInputGroup" placeholder="Enter Deisel Rate">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Taka/Ltr</div>
                                    </div>
                                </div>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">Petrol Rate</label>
    		                    <div class="input-group mb-3">
                                    <input type="text" required name="petrolRate" class="form-control" id="inlineFormInputGroup" placeholder="Enter Petrol Rate">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Taka/Ltr</div>
                                    </div>
                                </div>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">Octane Rate</label>
    		                    <div class="input-group mb-3">
                                    <input type="text" required name="octaneRate" class="form-control" id="inlineFormInputGroup" placeholder="Enter Octane Rate">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Taka/Ltr</div>
                                    </div>
                                </div>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">CNG Rate</label>
    		                    <div class="input-group mb-3">
                                    <input type="text" required name="cngRate" class="form-control" id="inlineFormInputGroup" placeholder="Enter CNG Rate">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Taka/Ltr</div>
                                    </div>
                                </div>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03 font-weight-bold">Order</label>
    		                    <input type="text" name="orderNumber" class="form-control" id="validationCustom03" placeholder="Enter next order number, keep blank for auto">
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">Active</label>
    		                    <input id="checkbox2" name="status" value="1" type="checkbox">
    		                </div>
    		                
                    	</div>
                    	
                    </div>
                <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
    setTimeout(function(){
      $('#alertShow').remove();
    }, 2000);
</script>
@endsection