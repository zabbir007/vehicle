@extends('layouts.admin')
@section('title') All Fuel Rate @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Fuel Rate Management</span>
                        <span>All Active Fuel Rate</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createFuelRate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Fuel Rate
                            </button>
                        </a>
                        <a href="{{route('allFuelRate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Fuel Rate
                            </button>
                        </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Branch</th>
                            <th>Deisel</th>
                            <th>Petrol</th>
                            <th>Octane</th>
                            <th>CNG</th>
                            <th>Entry Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fuelRateInfo as $fuelRate)
                        <tr>
                            <td>
                                <?php
                                    if ($fuelRate->branchName==1) {
                                        echo "Corporate";
                                    }else{
                                        echo "Motijheel";
                                    }
                                ?>
                            </td>
                            <td>{{$fuelRate->deiselRate}} Taka</td>
                            <td>{{$fuelRate->petrolRate}} Taka</td>
                            <td>{{$fuelRate->octaneRate}} Taka</td>
                            <td>{{$fuelRate->cngRate}} Taka</td>
                            <td>{{$fuelRate->entryDate}}</td>
                            <td>
                                <?php
                                    if ($fuelRate->status==1) {
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-success">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('editFuelRate',[$fuelRate->id])}}" title="Update Fuel Rate" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-square-edit-outline"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection