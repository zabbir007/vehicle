@extends('layouts.admin')
@section('title') View Company @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Company Management</span>
                        <span>Add New Company</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	<a href="{{route('editCompany',[$id])}}">
                            <button type="button" class="btn btn-primary waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-pen"></i></span>Update
                            </button>
                        </a>
                        <a href="{{route('createCompany')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Company
                            </button>
                        </a>
                        <a href="{{route('allCompany')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Companies
                            </button>
                        </a>
                        
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
	                <div class="row">
	                	<div class="col-6">
	                		<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Code</label>
			                    <input type="text" readonly="" value="{{$singleCompanyInfo->code}}" name="code" class="form-control" id="validationCustom03" placeholder="Optional Code" required>
			                    <div class="invalid-feedback">
			                        Please provide a code.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Name</label>
			                    <input type="text" readonly="" value="{{$singleCompanyInfo->name}}" name="name" class="form-control" id="validationCustom03" placeholder="Company Name" required>
			                    <div class="invalid-feedback">
			                        Please provide a company name.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Address</label>
			                    <textarea required readonly="" class="form-control" name="address" id="validationCustom03">{{$singleCompanyInfo->address}}</textarea>
			                    <div class="invalid-feedback">
			                        Please provide a optional address.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Email Suffix</label>
			                    <input type="text" readonly="" value="{{$singleCompanyInfo->emailSuffix}}" name="emailSuffix" class="form-control" id="validationCustom03" placeholder="Company email suffix" required>
			                    <div class="invalid-feedback">
			                        Please provide a email suffix.
			                    </div>
			                </div>  
	                	</div>
	                	
	                	<div class="col-6">
	                		<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Short Name</label>
			                    <input type="text" readonly="" value="{{$singleCompanyInfo->shortName}}" name="shortName" class="form-control" id="validationCustom03" placeholder="Company short name" required>
			                    <div class="invalid-feedback">
			                        Please provide a short name.
			                    </div>
			                </div>
			                
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Time Zone</label>
			                    <input type="text" name="timeZone" readonly="" value="{{$singleCompanyInfo->timeZone}}" class="form-control" id="validationCustom03" required>
			                    <div class="invalid-feedback">
			                        Please provide a timezone.
			                    </div>
			                </div>
	                	</div>
	                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 2000);
</script>
@endsection