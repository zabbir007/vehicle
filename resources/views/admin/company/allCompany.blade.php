@extends('layouts.admin')
@section('title') All Company @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Company Management</span>
                        <span>All Active Companies</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createCompany')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Company
                            </button>
                        </a>
                        <a href="{{route('allCompany')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Companies
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Short Name</th>
                            <th>Logo</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($companyInfo as $company)
                        <tr>
                            <td>{{$company->name}}</td>
                            <td>{{$company->shortName}}</td>
                            <td>
                            	<img src="{{asset( $company->logo )}}" alt="Transcom Distribution Company Limited" height="30px">
                            </td>
                            <td>{{$company->updated_at}}</td>
                            <td>
                                <?php
                                    if($company->status=='1'){
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-danger">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('viewCompany',[$company->id])}}" title="View Company" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editCompany',[$company->id])}}" title="Update Company" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                            <?php
                                if($company->status=='1'){
                            ?>
                                <a href="javascript:void(0);" id="{{$company->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnCompanyInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                            <?php
                                }else{
                            ?>
                            <a href="javascript:void(0);" id="{{$company->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnCompanyActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                            </td>
                            <?php
                                }
                            ?>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection