@extends('layouts.admin')
@section('title') Edit Company @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Company Management</span>
                        <span>Add New Company</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	<a href="{{route('viewCompany',[$id])}}">
                            <button type="button" class="btn btn-primary waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-pen"></i></span>View
                            </button>
                        </a>
                        <a href="{{route('createCompany')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Company
                            </button>
                        </a>
                        <a href="{{route('allCompany')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Companies
                            </button>
                        </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <form action="{{route('updateCompany')}}"  class="parsley-examples" enctype="multipart/form-data" method="post" novalidate>
                @csrf
                @if ($errors->any())
				    <div class="alert alert-danger" id="alertShow">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
                <?php 
                    $message=Session::get('message');
                    if($message){
                ?>
                    <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                            echo $message;
                            Session::put('message','');
                        ?>
                    </div>
                <?php
                	}
                ?>

                <?php 
                    $messageWarning=Session::get('messageWarning');
                    if($messageWarning){
                ?>
                    <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                            echo $messageWarning;
                            Session::put('messageWarning','');
                        ?>
                    </div>
                <?php
                	}
                ?>
	                <div class="row">
	                	<div class="col-6">
	                		<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Code</label>
			                    <input type="text" name="code" value="{{$singleCompanyInfo->code}}" class="form-control" id="validationCustom03" placeholder="Optional Code" required>
			                    <div class="invalid-feedback">
			                        Please provide a code.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Name</label>
			                    <input type="text" name="name" value="{{$singleCompanyInfo->name}}" class="form-control" id="validationCustom03" placeholder="Company Name" required>
			                    <div class="invalid-feedback">
			                        Please provide a company name.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Address</label>
			                    <textarea required class="form-control" name="address" id="validationCustom03">{{$singleCompanyInfo->address}}</textarea>
			                    <div class="invalid-feedback">
			                        Please provide a optional address.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Email Suffix</label>
			                    <input type="text" value="{{$singleCompanyInfo->emailSuffix}}" name="emailSuffix" class="form-control" id="validationCustom03" placeholder="Company email suffix" required>
			                    <div class="invalid-feedback">
			                        Please provide a email suffix.
			                    </div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03">Active</label>
			                    <input id="checkbox2" name="status" value="1" type="checkbox" checked>
			                </div>
			                <input type="hidden" name="id" value="{{$id}}">
	                	</div>
	                	
	                	<div class="col-6">
	                		<div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Short Name</label>
			                    <input type="text" name="shortName" value="{{$singleCompanyInfo->shortName}}" class="form-control" id="validationCustom03" placeholder="Company short name" required>
			                    <div class="invalid-feedback">
			                        Please provide a short name.
			                    </div>
			                </div>
			                <div class="row">
			                	<div class="col-6">
			                		<div class="form-group mb-3">
					                    <label for="validationCustom03 font-weight-bold">Update Logo</label>
					                    <input type="file" name="logo" class="form-control" id="validationCustom03" >
					                    <div class="invalid-feedback">
					                        Please provide a logo.
					                    </div>
					                </div>
			                	</div>
			                	<div class="col-6">
			                		<div class="form-group mb-3">
					                    <label for="validationCustom03 font-weight-bold">Present Logo</label></br>
					                    <img src="{{asset( $singleCompanyInfo->logo )}}" alt="Transcom Distribution Company Limited" height="30px" width="30px;">
					                </div>
			                		
			                	</div>
			                </div>
			                <div class="form-group mb-3">
			                    <label for="validationCustom03 font-weight-bold">Time Zone</label>
			                    <input type="text" name="timeZone" value="{{$singleCompanyInfo->timeZone}}" class="form-control" id="validationCustom03" required>
			                    <div class="invalid-feedback">
			                        Please provide a timezone.
			                    </div>
			                </div>
	                	</div>
	                	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Update Changes</button>
	                </div>
	            </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 2000);
</script>
@endsection