@extends('layouts.admin')
@section('title') Assign Branch @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Branch Management</span>
                        <span>Assign Branch</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	<a href="{{route('assignBranchCreate')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Assign Branch
	                        </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <form action="{{route('assignBranchSave')}}"  class="parsley-examples" method="post" novalidate>
	        	@csrf
	            @if ($errors->any())
				    <div class="alert alert-danger" id="alertShow">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
	            <?php 
	                $message=Session::get('message');
	                if($message){
	            ?>
	                <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
	            <?php
	            	}
	            ?>

	            <?php 
	                $messageWarning=Session::get('messageWarning');
	                if($messageWarning){
	            ?>
	                <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $messageWarning;
	                        Session::put('messageWarning','');
	                    ?>
	                </div>
	            <?php
	            	}
	            ?>
                <div class="row">
                	<div class="col-6">
                		<div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Branch</label>
		                    <select id="heard" class="form-control" required name="branchId" data-toggle="select2">
	                            <option value="">Select</option>
	                            @foreach($branchInfo as $branch)
	                            	<option value="{{$branch->id}}">{{$branch->name}}</option>
	                            @endforeach
	                        </select>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Employee</label>
		                    <select id="heard" class="form-control" required name="employeeId" data-toggle="select2">
	                            <option value="">Select</option>
	                            @foreach($employeeInfo as $employee)
	                            	<option value="{{$employee->id}}">{{$employee->firstName}}</option>
	                            @endforeach
	                        </select>
		                </div>
		                
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Active</label>
		                    <input id="checkbox2" name="status" value="1" type="checkbox" checked>
		                </div>
                	</div>
                	
                </div>
                <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
            	</form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 5000);
</script>
@endsection