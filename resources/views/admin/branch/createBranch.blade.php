@extends('layouts.admin')
@section('title') Create Branch @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Branch Management</span>
                        <span>Add New Branch</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	<a href="{{route('createBranch')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Branch
	                        </button>
                        </a>
                        <a href="{{route('allBranch')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-th-list"></i></span>All Branches
	                        </button>
	                    </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <form action="{{route('saveBranch')}}"  class="parsley-examples" method="post" novalidate>
	        	@csrf
	            @if ($errors->any())
				    <div class="alert alert-danger" id="alertShow">
				        <ul>
				            @foreach ($errors->all() as $error)
				                <li>{{ $error }}</li>
				            @endforeach
				        </ul>
				    </div>
				@endif
	            <?php 
	                $message=Session::get('message');
	                if($message){
	            ?>
	                <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $message;
	                        Session::put('message','');
	                    ?>
	                </div>
	            <?php
	            	}
	            ?>

	            <?php 
	                $messageWarning=Session::get('messageWarning');
	                if($messageWarning){
	            ?>
	                <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
	                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                        <span aria-hidden="true">&times;</span>
	                    </button>
	                    <?php
	                        echo $messageWarning;
	                        Session::put('messageWarning','');
	                    ?>
	                </div>
	            <?php
	            	}
	            ?>
                <div class="row">
                	<div class="col-6">
                		<div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Code</label>
		                    <input type="text" class="form-control" name="code" id="validationCustom03" placeholder="Optional Code" required>
		                    <div class="invalid-feedback">
		                        Please provide a code.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Name</label>
		                    <input type="text" name="name" class="form-control" id="validationCustom03" placeholder="Branch Name" required>
		                    <div class="invalid-feedback">
		                        Please provide a branch name.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Address1</label>
		                    <textarea required class="form-control" name="addressOne" id="validationCustom03"></textarea>
		                    <div class="invalid-feedback">
		                        Please provide a optional address.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Email</label>
		                    <input type="text" name="email" class="form-control" id="validationCustom03" placeholder="Branch Email" required>
		                    <div class="invalid-feedback">
		                        Please provide a email suffix.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Active</label>
		                    <input id="checkbox2" name="status" value="1" type="checkbox" checked>
		                </div>
		                
                	</div>
                	
                	<div class="col-6">
                		<div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Short Name</label>
		                    <input type="text" name="shortName" class="form-control" id="validationCustom03" placeholder="Branch short name" required>
		                    <div class="invalid-feedback">
		                        Please provide a short name.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Address2</label>
		                    <textarea class="form-control" name="addressSecond" id="validationCustom03"></textarea>
		                    
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Order</label>
		                    <input type="text" name="order" class="form-control" id="validationCustom03" placeholder="Enter next order number, keep blank for auto">
		                    
		                </div>
                	</div>
                	<button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
                </div>
            	</form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 5000);
</script>
@endsection