@extends('layouts.admin')
@section('title') All Branch @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Branch Management</span>
                        <span>All Active Branches</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createBranch')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Branch
                            </button>
                        </a>
                        <a href="{{route('allBranch')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Branches
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Short Name</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($branchInfo as $branch)
                        <tr>
                            <td>{{$branch->name}}</td>
                            <td>{{$branch->shortName}}</td>
                            <td>{{$branch->updateDate}}</td>
                            <td>
                                <?php
                                    if ($branch->status==1) {
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-success">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('viewBranch',[$branch->id])}}" title="View Company" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('editBranch',[$branch->id])}}" title="Update Company" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <a href="javascript:void(0);" title="Trash Branch" id="{{$branch->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnBranchDelete"> <i class="fas fa-trash"></i></a>
                                <?php
                                if($branch->status=='1'){
                                ?>
                                    <a href="javascript:void(0);" id="{{$branch->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnBranchInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }else{
                                ?>
                                <a href="javascript:void(0);" id="{{$branch->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnBranchActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection