@extends('layouts.admin')
@section('title') View Branch @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Branch Management</span>
                        <span>View Branch</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                    	
                    	<a href="{{route('editBranch',[$id])}}">
	                        <button type="button" class="btn btn-primary waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Update
	                        </button>
                        </a>
                    	<a href="{{route('createBranch')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Branch
	                        </button>
                        </a>
                        <a href="{{route('allBranch')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-th-list"></i></span>All Branches
	                        </button>
	                    </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <div class="row">
                	<div class="col-6">
                		<div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Code</label>
		                    <input type="text" class="form-control" disabled value="{{$singleBranchInfo->code}}" name="code" id="validationCustom03" placeholder="Optional Code" required>
		                    <div class="invalid-feedback">
		                        Please provide a code.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Name</label>
		                    <input type="text" name="name" disabled value="{{$singleBranchInfo->name}}" class="form-control" id="validationCustom03" placeholder="Branch Name" required>
		                    <div class="invalid-feedback">
		                        Please provide a branch name.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Address1</label>
		                    <textarea required class="form-control" name="addressOne" disabled id="validationCustom03">{{$singleBranchInfo->addressOne}}</textarea>
		                    <div class="invalid-feedback">
		                        Please provide a optional address.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Email</label>
		                    <input type="text" disabled value="{{$singleBranchInfo->email}}" name="email" class="form-control" id="validationCustom03" placeholder="Branch Email" required>
		                    <div class="invalid-feedback">
		                        Please provide a email suffix.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Active</label>
		                    <input id="checkbox2" disabled name="status" <?php if($singleBranchInfo->status=='1'){echo "checked";} ?> value="1" type="checkbox">
		                </div>
		                
                	</div>
                	
                	<div class="col-6">
                		<div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Short Name</label>
		                    <input type="text" disabled value="{{$singleBranchInfo->shortName}}" name="shortName" class="form-control" id="validationCustom03" placeholder="Branch short name" required>
		                    <div class="invalid-feedback">
		                        Please provide a short name.
		                    </div>
		                </div>
		                <div class="form-group mb-3">
		                    <label for="validationCustom03">Address2</label>
		                    <textarea class="form-control" disabled name="addressSecond" id="validationCustom03">{{$singleBranchInfo->addressSecond}}</textarea>
		                    
		                </div>
		                <input type="hidden" name="id" value="{{$singleBranchInfo->id}}">
		                <div class="form-group mb-3">
		                    <label for="validationCustom03 font-weight-bold">Order</label>
		                    <input type="text" disabled name="order" value="{{$singleBranchInfo->order}}" class="form-control" id="validationCustom03" placeholder="Enter next order number, keep blank for auto">
		                    
		                </div>
                	</div>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
	setTimeout(function(){
	  $('#alertShow').remove();
	}, 5000);
</script>
@endsection