@extends('layouts.admin')
@section('title') Assign Branch @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> Branch User Management</span>
                        <span>Assign Branch Users</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('assignBranchCreate')}}">
	                        <button type="button" class="btn btn-success waves-effect waves-light">
	                            <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Assign Branch
	                        </button>
                        </a>
                        <button type="button" class="btn btn-success waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-th-list"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th scope="col">User</th>
				      <th scope="col">Branches</th>
				      <th>Action</th>
				    </tr>
				  </thead>
				  <tbody>
				  	@foreach($assignInfo as $assign)
				    <tr>	
				      <td>{{$assign->firstName}}</td>
				      <td>{{$assign->name}}</td>
				      <td>
				      	<a href="{{route('assignBranchEdit',[$assign->id])}}" title="Update Branch" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
				      	<a href="javascript:void(0);" title="Trash Driver" id="{{$assign->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnBranchAssignDelete"> <i class="fas fa-trash"></i></a>
				      </td>
				    </tr>
				    @endforeach
				  </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection