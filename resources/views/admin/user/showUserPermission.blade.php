@extends('layouts.admin')
@section('title') User Permission @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Permission Manager</span>
                        <span>All Active Permissions</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('createUserPermission')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Permission
                            </button>
                        </a>
                        <a href="{{route('showUserPermission')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Permission
                            </button>
                        </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Permission</th>
                            <th>Identifier</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($permissionInfo as $permission)
                        <tr>
                            <td>{{$permission->permission}}</td>
                            <td>{{$permission->identifire}}</td>
                            <td>{{$permission->lastUpdate}}</td>
                            <td>
                                <?php
                                    if ($permission->status==1) {
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-danger">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('editUserPermission',[$permission->id])}}" title="Update User" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <?php
                                if($permission->status=='1'){
                                ?>
                                    <a href="javascript:void(0);" id="{{$permission->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnPermissionInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }else{
                                ?>
                                <a href="javascript:void(0);" id="{{$permission->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnPermissionActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }
                                ?>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection