@extends('layouts.admin')
@section('title') All User @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> User Management</span>
                        <span>All Active User</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{Route('userCreate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create User
                            </button>
                        </a>
                        <a href="{{Route('showUser')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Users
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th>Roles</th>
                            <th>Last Updated</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($UserInfo as $user)
                        <tr>
                            <td>{{$user->name}}</td>
                            <td>{{$user->userName}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->roleName}}</td>
                            <td>{{$user->lastUpdate}}</td>
                            <td>
                                <?php
                                    if ($user->status==1) {
                                ?>
                                <h5><span class="badge badge-success">Active</span></h5>
                                <?php
                                    }else{
                                ?>
                                <h5><span class="badge badge-success">De-Active</span></h5>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <a href="{{route('userView',[$user->id])}}" title="View User" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="mdi mdi-eye"></i></a>
                                <a href="{{route('userEdit',[$user->id])}}" title="Update User" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary"> <i class="mdi mdi-square-edit-outline"></i></a>
                                <?php
                                if($user->status=='1'){
                                ?>
                                    <a href="javascript:void(0);" id="{{$user->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-primary bg-gradient-primary btnUserInActive" title="Set to Inactive"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }else{
                                ?>
                                <a href="javascript:void(0);" id="{{$user->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-primary btnUserActive" title="Set to Active"> <i class="fas fa-ban"></i></a>
                                <?php
                                    }
                                ?>
                                <a href="{{route('userLoginAs',[$user->id])}}" title="Login As {{$user->userName}}" class="btn btn-xs btn-flat btn-square mr-1 btn-info bg-gradient-info"> <i class="fas fa-user-secret"></i></a>
                                <a href="javascript:void(0);" title="Change User Password" class="btn btn-xs btn-flat btn-square mr-1 btn-warning bg-gradient-warning"> <i class="fas fa-lock"></i></a>
                                <a href="javascript:void(0);" title="Trash Employee" id="{{$user->id}}" class="btn btn-xs btn-flat btn-square mr-1 btn-danger bg-gradient-danger btnUserDelete"> <i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <hr style="border-top: 1px dashed black;">
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection