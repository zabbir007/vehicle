@extends('layouts.admin')
@section('title') Create Role @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-user-lock"></i></span>
                        <span style="color: black;font-size: 130%"> Role Manager</span>
                        <span>Add New Role</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{route('userRoleCreate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create Role
                            </button>
                        </a>
                        <a href="{{route('showUserRole')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Roles
                            </button>
                        </a>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                <form action="{{route('userRoleSave')}}"  class="parsley-examples" method="post" novalidate>
                @csrf
                    @if ($errors->any())
                        <div class="alert alert-danger" id="alertShow">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <?php 
                        $message=Session::get('message');
                        if($message){
                    ?>
                        <div style="margin-top: 40px;" id="alertShow" class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $message;
                                Session::put('message','');
                            ?>
                        </div>
                    <?php
                        }
                    ?>

                    <?php 
                        $messageWarning=Session::get('messageWarning');
                        if($messageWarning){
                    ?>
                        <div style="margin-top: 40px;" id="alertShow" class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php
                                echo $messageWarning;
                                Session::put('messageWarning','');
                            ?>
                        </div>
                    <?php
                        }
                    ?>
                    <div class="row">
                    	<div class="col-6">
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03 font-weight-bold">Role Name</label>
    		                    <input type="text" class="form-control" name="roleName" id="validationCustom03" placeholder="Super-Admin" required>
    		                    <div class="invalid-feedback">
    		                        Please provide a role name.
    		                    </div>
    		                </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">Active</label>
    		                    <input id="checkbox2" name="status" value="1" type="checkbox" checked>
    		                </div>
                    	</div>
                    	<div class="col-2">
                    		<h5 for="" style="color: red;">Access All Module </h5>
                    		<div class="form-group mb-3">
    		                    <label for="validationCustom03">All Access</label>
    		                    <input id="checkbox2" name="all" value="1" type="checkbox">
    		                </div>
                    	</div>
                    	<div class="col-4">
                    		<h5 for="" style="color: green;">Individual Access Module </h5>
                            @foreach($permissionInfo as $permission)
                    		<div class="form-group mb-3">
                    			<div class="row">
                    				<div class="col-6">
                    					<label for="validationCustom03">{{$permission->permission}}</label>
                    				</div>
                    				<div class="col-6">
                    					<input id="checkbox2" name="rolePermission[]" value="{{$permission->identifire}}" type="checkbox">
                    				</div>
                    			</div>
    		                </div>
    		                @endforeach
    		                
                    	</div>
                    </div>
                <button type="submit" class="btn btn-success btn-rounded waves-effect waves-light">Save Changes</button>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
    setTimeout(function(){
      $('#alertShow').remove();
    }, 2000);
</script>
@endsection