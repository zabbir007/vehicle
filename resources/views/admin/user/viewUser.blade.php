@extends('layouts.admin')
@section('title') Create User @endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <span style="color: black;font-size: 150%"><i class="fas fa-users"></i></span>
                        <span style="color: black;font-size: 130%"> User Management</span>
                        <span>View Active User</span>
                    </div>
                    <div class="col-6" style="text-align: left;">
                        <a href="{{Route('userCreate')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-plus-circle"></i></span>Create User
                            </button>
                        </a>
                        <a href="{{Route('showUser')}}">
                            <button type="button" class="btn btn-success waves-effect waves-light">
                                <span class="btn-label"><i class="fas fa-th-list"></i></span>All Users
                            </button>
                        </a>
                        <button type="button" class="btn btn-primary waves-effect waves-light">
                            <span class="btn-label"><i class="fas fa-cogs"></i></span>More
                        </button>
                    </div>
                </div>
                <hr style="border-top: 1px dashed black;">
                    <div class="row">
                    	<div class="col-6">
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03 font-weight-bold">Name</label>
    		                    <input type="text" disabled class="form-control" value="{{$userInfo->name}}" name="name" id="validationCustom03" placeholder="Name">
    		                </div>
                            <div class="form-group mb-3">
                                <label for="validationCustom03 font-weight-bold">UserName</label>
                                <input type="text" disabled class="form-control" name="userName" value="{{$userInfo->userName}}" id="validationCustom03" placeholder="UserName" required>
                                <div class="invalid-feedback">
                                    Please provide a name.
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="validationCustom03 font-weight-bold">Email</label>
                                <input type="text" disabled class="form-control" value="{{$userInfo->email}}" name="email" id="validationCustom03" placeholder="Email" required>
                                <div class="invalid-feedback">
                                    Please provide a email.
                                </div>
                            </div>
    		                <div class="form-group mb-3">
    		                    <label for="validationCustom03">Active</label>
    		                    <input id="checkbox2" disabled name="status" value="1" <?php if($userInfo->status=='1'){echo "checked";} ?> type="checkbox" checked>
    		                </div>
                    	</div>
                        <div class="col-6">
                            @foreach($roleInfo as $role)
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label for="validationCustom03">{{$role->roleName}}</label>
                                    <input id="checkbox2" <?php if($userInfo->roleId==$role->id){echo "checked";} ?> name="roleId" value="{{$role->id}}" type="radio" required>
                                </div>
                                <label for="validationCustom03">All Management</label>
                                <input id="checkbox2" disabled <?php if($role->all=='1'){echo "checked";} ?> value="1" type="checkbox"><br>
                                <label for="validationCustom03">Individual Access</label>
                                <p>{{$role->rolePermission}}</p>
                            </div>
                            @endforeach
                        </div>
                    </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
<script>
    setTimeout(function(){
      $('#alertShow').remove();
    }, 2000);
</script>
@endsection