========== Left Sidebar Start ========== -->
            <div class="left-side-menu">

                <div class="slimscroll-menu">

                    <!-- User box -->
                    <div class="user-box text-center">
                        <img src="{{asset('admin/assets/images/logo.png')}}" alt="logo" title="" class="rounded-circle avatar-md">
                        <div class="dropdown">
                            <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{Session::get("adminUserName")}}</a>
                            <div class="dropdown-menu user-pro-dropdown">

                                    <i class="fe-user mr-1"></i>
                                    <span>
                                        <?php
                                            $role=Session::get("adminRole");
                                            if ($role=="1") {
                                                echo "Super Admin";
                                            }
                                        ?>
                                    </span>
                                </a>
                                <!-- item-->
                                <a href="{{route('superAdminLogout')}}" class="dropdown-item notify-item">
                                    <i class="fe-log-out mr-1"></i>
                                    <span>Logout</span>
                                </a>
    
                            </div>
                        </div>
                        <p class="text-muted">
                            {{Session::get("adminUserName")}}                            
                        </p>
                    </div>

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title"></li>

                            <li>
                                <a href="{{route('superAdminDashboard')}}">
                                    <i class="fe-airplay"></i>
                                    
                                    <span> Dashboards </span>
                                </a>
                            </li>
                            <?php
                                $all=Session::get("all");
                                if ($all=='1') {
                            ?>
                            <li>
                                <a href="#">
                                    <span class="" style="color: black;size: 15px;"> Preferences </span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="nav-icon fas fa-users-cog"></i>
                                    <span> Access Management </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showUser')}}">User Manager</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showUserRole')}}">Role Manager</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showUserPermission')}}">Permission Manager</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="nav-icon fas fa-external-link-alt"></i>
                                    <span> API Management </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">API Clients</a>
                                    </li>
                                    <li>
                                        <a href="#">API Tokens</a>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="#">
                                    <span class="align-right" style="color: black;size: 15px;"> Log Manager </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="nav-icon fas fa-user-secret"></i>
                                    <span> System Logs </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('showLogDashboard')}}">Log Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="{{route('showAllLog')}}">All Logs</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#">
                                    <span class="align-right" style="color: black;size: 15px;"> Master Setup </span>
                                </a>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-building"></i>
                                    <span> Company Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('allCompany')}}">All Company</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createCompany')}}">New Company</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-users-cog"></i>
                                    <span> Branch Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('assignBranch')}}">Assign Branch</a>
                                    </li>
                                    <li>
                                        <a href="{{route('allBranch')}}">All Branch</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createBranch')}}">New Branch</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-users-cog"></i>
                                    <span> Employee Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('allEmployee')}}">All Employees</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createEmployee')}}">New Employee</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-bus"></i>
                                    <span> Driver Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('allDriver')}}">All Drivers</a>
                                    </li>
                                    <li>
                                        <a href="{{route('driverCreate')}}">New Driver</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-truck"></i>
                                    <span> Vehicle Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('assignVehicle')}}">Assign Driver</a>
                                    </li>
                                    <li>
                                        <a href="{{route('allVehicle')}}">All Vehicles</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createVehicle')}}">New Vehicles</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="align-right" style="color: black;size: 15px;"> Transactions </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-adjust"></i>
                                    <span> Fuel Rates </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="{{route('allFuelRate')}}">All Fuel Rates</a>
                                    </li>
                                    <li>
                                        <a href="{{route('createFuelRate')}}">New Fuel</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-th-list"></i>
                                    <span> Logbook Entries</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">All Logbook Entries</a>
                                    </li>
                                    <li>
                                        <a href="#">New Logbook Entry</a>
                                    </li>
                                </ul>
                            </li>
                            <?php
                                }else{
                            ?>
                            <li>
                                <a href="#">
                                    <span class="" style="color: black;size: 15px;"> Preferences </span>
                                </a>
                            </li>
                           
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="nav-icon fas fa-users-cog"></i>
                                    <span> Access Management</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueshowUser");
                                        if ($access=='showUser') {
                                    ?>
                                    <li>
                                        <a href="{{route('showUser')}}">User Manager</a>
                                    </li>
                                    <?php
                                        }
                                    ?>

                                    <?php
                                        $access=Session::get("accessValueshowUserRole");
                                        if ($access=='showUserRole') {
                                    ?>
                                    <li>
                                        <a href="{{route('showUserRole')}}">Role Manager</a>
                                    </li>
                                    <?php
                                        }
                                    ?>

                                    <?php
                                        $access=Session::get("accessValueshowUserPermission");
                                        if ($access=='showUserPermission') {
                                    ?>
                                    <li>
                                        <a href="{{route('showUserPermission')}}">Permission Manager</a>
                                    </li>
                                     <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                           

                           
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="nav-icon fas fa-external-link-alt"></i>
                                    <span> API Management </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">API Clients</a>
                                    </li>
                                    <li>
                                        <a href="#">API Tokens</a>
                                    </li>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="#">
                                    <span class="align-right" style="color: black;size: 15px;"> Log Manager </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="nav-icon fas fa-user-secret"></i>
                                    <span> System Logs </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueshowLogDashboard");
                                        if ($access=='showLogDashboard') {
                                    ?>
                                    <li>
                                        <a href="{{route('showLogDashboard')}}">Log Dashboard</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValueshowAllLog");
                                        if ($access=='showAllLog') {
                                    ?>
                                    <li>
                                        <a href="{{route('showAllLog')}}">All Logs</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="#">
                                    <span class="align-right" style="color: black;size: 15px;"> Master Setup </span>
                                </a>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-building"></i>
                                    <span> Company Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueallCompany");
                                        if ($access=='allCompany') {
                                    ?>
                                    <li>
                                        <a href="{{route('allCompany')}}">All Company</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValuecreateCompany");
                                        if ($access=='createCompany') {
                                    ?>
                                    <li>
                                        <a href="{{route('createCompany')}}">New Company</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-users-cog"></i>
                                    <span> Branch Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueassignBranch");
                                        if ($access=='assignBranch') {
                                    ?>
                                    <li>
                                        <a href="{{route('assignBranch')}}">Assign Branch</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValueallBranch");
                                        if ($access=='allBranch') {
                                    ?>
                                    <li>
                                        <a href="{{route('allBranch')}}">All Branch</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValuecreateBranch");
                                        if ($access=='createBranch') {
                                    ?>
                                    <li>
                                        <a href="{{route('createBranch')}}">New Branch</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-users-cog"></i>
                                    <span> Employee Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueallEmployee");
                                        if ($access=='allEmployee') {
                                    ?>
                                    <li>
                                        <a href="{{route('allEmployee')}}">All Employees</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValuecreateEmployee");
                                        if ($access=='createEmployee') {
                                    ?>
                                    <li>
                                        <a href="{{route('createEmployee')}}">New Employee</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                           
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-bus"></i>
                                    <span> Driver Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueallDriver");
                                        if ($access=='allDriver') {
                                    ?>
                                    <li>
                                        <a href="{{route('allDriver')}}">All Drivers</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValuedriverCreate");
                                        if ($access=='driverCreate') {
                                    ?>
                                    <li>
                                        <a href="{{route('driverCreate')}}">New Driver</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-truck"></i>
                                    <span> Vehicle Manager </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueassignVehicle");
                                        if ($access=='assignVehicle') {
                                    ?>
                                    <li>
                                        <a href="{{route('assignVehicle')}}">Assign Driver</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValueallVehicle");
                                        if ($access=='allVehicle') {
                                    ?>
                                    <li>
                                        <a href="{{route('allVehicle')}}">All Vehicles</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValuecreateVehicle");
                                        if ($access=='createVehicle') {
                                    ?>
                                    <li>
                                        <a href="{{route('createVehicle')}}">New Vehicles</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="#">
                                    <span class="align-right" style="color: black;size: 15px;"> Transactions </span>
                                </a>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-adjust"></i>
                                    <span> Fuel Rates </span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <?php
                                        $access=Session::get("accessValueallFuelRate");
                                        if ($access=='allFuelRate') {
                                    ?>
                                    <li>
                                        <a href="{{route('allFuelRate')}}">All Fuel Rates</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        $access=Session::get("accessValuecreateFuelRate");
                                        if ($access=='createFuelRate') {
                                    ?>
                                    <li>
                                        <a href="{{route('createFuelRate')}}">New Fuel</a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </li>
                            
                            <li>
                                <a href="javascript: void(0);">
                                    <i class="fas fa-th-list"></i>
                                    <span> Logbook Entries</span>
                                    <span class="menu-arrow"></span>
                                </a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li>
                                        <a href="#">All Logbook Entries</a>
                                    </li>
                                    <li>
                                        <a href="#">New Logbook Entry</a>
                                    </li>
                                </ul>
                            </li>
                           
                            <?php
                                }
                            ?>
                        </ul>

                    </div>
                    <!-- End Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End