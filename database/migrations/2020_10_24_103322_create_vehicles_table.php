<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->integer("companyId");
            $table->string("name");
            $table->string("manfYear");
            $table->integer("driverId")->nullable();
            $table->string("weight")->nullable();
            $table->string("licenseNo");
            $table->string("chassisNo");
            $table->string("vinNo")->nullable();
            $table->string("opeMill")->nullable();
            $table->string("status")->default(0);
            $table->string("vehicleType");
            $table->string("manufacture")->nullable();
            $table->string("model");
            $table->string("lifeTime")->nullable();
            $table->string("licenseYear")->nullable();
            $table->string("engineNo");
            $table->string("purDate")->nullable();
            $table->integer("order")->nullable();
            $table->string("fitDocumentType")->nullable();
            $table->string("fitDocumentNum")->nullable();
            $table->string("fitIssue")->nullable();
            $table->string("fitIssuing")->nullable();
            $table->string("fitExpire")->nullable();
            $table->string("insDocumentType")->nullable();
            $table->string("insDocumentNum")->nullable();
            $table->string("insIssue")->nullable();
            $table->string("insIssuing")->nullable();
            $table->string("insExpire")->nullable();
            $table->string("taxDocumentType")->nullable();
            $table->string("taxDocumentNum")->nullable();
            $table->string("taxIssue")->nullable();
            $table->string("taxIssuing")->nullable();
            $table->string("taxExpire")->nullable();
            $table->string("fuelEntryDate")->nullable();
            $table->integer("dualTank")->nullable();
            $table->string("firstFuel")->nullable();
            $table->string("firstCapacity")->nullable();
            $table->string("firstStandard")->nullable();
            $table->string("secondFuel")->nullable();
            $table->string("secondCapacity")->nullable();
            $table->string("secondStandard")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
