<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("birth");
            $table->string("blood")->nullable();
            $table->string("presentAddress");
            $table->string("photo")->nullable();
            $table->integer("driverId");
            $table->string("mobile");
            $table->string("joining");
            $table->string("emMobile")->nullable();
            $table->string("permanentAddress");
            $table->string("orderNumber")->nullable();
            $table->string("status")->default(0);
            $table->string("nidDocument")->nullable();
            $table->string("nidIssuing")->nullable();
            $table->string("nidNumber")->nullable();
            $table->string("nidExpire")->nullable();
            $table->string("nidIssue")->nullable();
            $table->string("drivingDocument")->nullable();
            $table->string("drivingIssuing")->nullable();
            $table->string("drivingNumber")->nullable();
            $table->string("drivingExpire")->nullable();
            $table->string("drivingIssue")->nullable();
            $table->string("updateDate")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
