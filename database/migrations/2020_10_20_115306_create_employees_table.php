<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->integer("companyId");
            $table->string("firstName");
            $table->string("designation");
            $table->string("email");
            $table->integer("employeeId");
            $table->string("lastName");
            $table->string("department");
            $table->string("mobile");
            $table->string("order");
            $table->string("branch")->nullable();
            $table->integer("status")->default(0);
            $table->integer("loginStatus")->default(0);
            $table->string("userName")->nullable();
            $table->string("password")->nullable();
            $table->string("updateDate")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
