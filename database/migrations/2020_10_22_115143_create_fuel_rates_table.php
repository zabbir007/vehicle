<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuelRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fuel_rates', function (Blueprint $table) {
            $table->id();
            $table->integer("branchName");
            $table->string("entryDate");
            $table->string("deiselRate");
            $table->string("petrolRate");
            $table->string("octaneRate");
            $table->string("cngRate");
            $table->string("orderNumber")->nullable();
            $table->integer("status")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fuel_rates');
    }
}
