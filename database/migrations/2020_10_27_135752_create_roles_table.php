<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string("roleName");
            $table->integer("all")->nullable();
            $table->integer("access")->nullable();
            $table->integer("api")->nullable();
            $table->integer("system")->nullable();
            $table->integer("company")->nullable();
            $table->integer("branch")->nullable();
            $table->integer("employee")->nullable();
            $table->integer("driver")->nullable();
            $table->integer("vehicle")->nullable();
            $table->integer("fuel")->nullable();
            $table->integer("logbook")->nullable();
            $table->integer("status")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
